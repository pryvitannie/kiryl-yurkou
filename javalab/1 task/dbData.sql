--------------------------------------------------------
--  File created - Thursday-January-21-2016   
--------------------------------------------------------
REM INSERTING into AUTHORS
SET DEFINE OFF;
Insert into AUTHORS (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (49,'Моисеенко',null);
Insert into AUTHORS (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (9,'Павлович',null);
Insert into AUTHORS (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (50,'Журавский',null);
Insert into AUTHORS (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (51,'Юрков',null);
Insert into AUTHORS (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (0,'Новый автор',null);
Insert into AUTHORS (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (52,'Michael',null);
Insert into AUTHORS (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (53,'Michael',null);
Insert into AUTHORS (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (54,'Michael',null);
Insert into AUTHORS (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (55,'Vladislav',null);
Insert into AUTHORS (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (56,'Vladislav',null);
Insert into AUTHORS (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (57,'Vladislav',null);
Insert into AUTHORS (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (48,'Гусаков',null);
Insert into AUTHORS (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (58,'Vladislav',null);
Insert into AUTHORS (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (59,'Vladislav',null);
Insert into AUTHORS (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (60,'Vladislav',null);
Insert into AUTHORS (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (61,'Vladislav',null);
REM INSERTING into COMMENTS
SET DEFINE OFF;
Insert into COMMENTS (COMMENT_ID,COMMENT_TEXT,CREATION_DATE,NEWS_ID) values (10,'RIP',to_timestamp('11-JAN-16 03.38.46.606000000 PM','DD-MON-RR HH.MI.SSXFF AM'),66);
Insert into COMMENTS (COMMENT_ID,COMMENT_TEXT,CREATION_DATE,NEWS_ID) values (11,'rest in peace....',to_timestamp('11-JAN-16 03.39.00.181000000 PM','DD-MON-RR HH.MI.SSXFF AM'),66);
Insert into COMMENTS (COMMENT_ID,COMMENT_TEXT,CREATION_DATE,NEWS_ID) values (12,'What a huge loss...',to_timestamp('11-JAN-16 03.39.49.397000000 PM','DD-MON-RR HH.MI.SSXFF AM'),66);
REM INSERTING into NEWS
SET DEFINE OFF;
Insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (66,'Умер Дэвид Боуи','Британский музыкант Дэвид Боуи скончался от рака в возрасте 69 лет.
','«Дэвид Боуи скончался в окружении своей семьи после борьбы с раком, продлившейся 18 месяцев. Хотя многие из вас сожалеют об этой утрате, мы просим вас уважать права членов его семьи», — говорится в сообщении на странице в Facebook.',to_timestamp('11-JAN-16 06.13.13.373000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('11-JAN-16','DD-MON-RR'));
Insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (67,'Беларусь МВФ','Беларусь рассчитывает начать новую программу с Международным валютным фондом в январе-феврале.','«Надеемся, что в ближайшее время войдем в программу с фондом — в январе — начале февраля», — отметил в беседе с журналистами первый вице-премьер.
Василий Матюшевский напомнил, что недавно в Беларуси работала очередная миссия Международного валютного фонда. Подготовлено видение новой программы расширенного финансирования, оставался один принципиальный вопрос — тарифов ЖКХ. На данный момент работа по нему практически завершена. «Есть видение по этому направлению, включая ряд компенсационных действий для отдельных категорий граждан», — проинформировал первый вице-премьер.
Напомним, Минск обсуждает возможность привлечения кредита МВФ в сумме $ 3 млрд под 2,28% сроком на 10 лет. В МВФ говорят о том, что новый кредит Беларусь получит только в случае готовности Минска двигаться по пути глубоких реформ, направленных на повышение эффективности национальной экономики. Белорусское правительство при поддержке Всемирного банка подготовило и передало МВФ так называемую дорожную карту по структурному реформированию экономики. Речь шла о сокращении финансирования госпрограмм, реформировании сектора госпредприятий, ликвидации перекрестного субсидирования, повышении тарифов на транспорт и жилищно-коммунальные услуги. МВФ рекомендовал Минску провести ключевые реформы в течение 6−18 месяцев.',to_timestamp('11-JAN-16 03.22.24.442000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('11-JAN-16','DD-MON-RR'));
Insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (23,'Terror in Paris','Terrotists attacks Paris','On the evening of 13 November 2015, a series of coordinated Islamic terrorist attacks occurred in Paris and its northern suburb, Saint-Denis. Beginning at 21:20 CET, three suicide bombers struck near the Stade de France in Saint-Denis, followed by suicide bombings and mass shootings at cafés, restaurants, and a concert hall in Paris.
The attackers killed 130 people, including 89 at the Bataclan theatre, where they took hostages before engaging in a stand-off with police. There were 368 people who were wounded, 80–99 seriously so. 
Seven of the attackers also died, while authorities continued to search for accomplices.
The attacks were the deadliest on France since World War II, and the deadliest in the European Union since the Madrid train bombings in 2004. France had been on high alert since the January 2015 attacks in Paris that killed 17 people, including civilians and police officers.',to_timestamp('18-NOV-15 12.52.20.783000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('18-NOV-15','DD-MON-RR'));
Insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (68,'Падение цен на нефть на 25%','Аналитики Morgan Stanley предсказали падение цен на нефть еще на 25%.
','Быстрое укрепление американской валюты может привести к падению стоимости нефти до 20 долларов за баррель, считают аналитики Morgan Stanley. Они утверждают, что в снижении цен ниже 60 долларов виноваты доллар и косвенные факторы.
Нефть весьма чувствительна к доллару и может обвалиться еще на 10−25%, если американская валюта окрепнет на 5%, пишет Bloomberg, ознакомившийся с аналитической запиской экспертов банка Morgan Stanley, датированной 11 января. В документе говорится, что до 60 долларов за баррель нефть упала вследствие затоваренности рынка, но дальнейшим снижением до 35 долларов она обязана в основном доллару.
«С учетом продолжающегося укрепления американского доллара возможны сценарии с ценой нефти 20−25 долларов, что произойдет исключительно благодаря валюте, — утверждают аналитики. — На стоимость нефти продолжают влиять доллар США и нефундаментальные факторы».
Эксперты Morgan Stanley подчеркнули, что нефть по 20 долларов возможна, но не по тем причинам, которые называются чаще всего. «Речь идет не об ухудшении базовых факторов», — отмечается в записке.
Аналитики банка пояснили, что при укреплении доллара на 3,2%, что следует из возможной девальвации юаня на 15%, нефть будет стоить немногим ниже 30 долларов за баррель, но если за китайской валютой последуют другие, то доллар пойдет дальше в рост, а стоимость барреля продолжит снижаться.
Ранее снижение стоимости нефти до 20 долларов предсказывал банк Goldman Sachs. Его эксперты объясняли это тем, что емкости нефтехранилищ будут заполнены, и цены должны упасть до уровня, при котором некоторые производители будут вынуждены остановить добычу. По данным Минэнерго США, в крупнейшем американском нефтехранилище в Кушинге, штат Оклахома, к 1 январю запасы достигли 63,9 млн баррелей при расчетной вместимости 73 млн.
',to_timestamp('11-JAN-16 03.25.27.372000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('11-JAN-16','DD-MON-RR'));
Insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (69,'Java — язык года','Java назван языком 2015 года по версии голландской компании TIOBE','Индекс TIOBE оценивает ежемесячную популярность языков программирования на основе подсчёта результатов поисковых запросов на наиболее посещаемых сайтах (Google, Wikipedia, Baidu, Yahoo!, Bing и др.). Он подразумевает наличие корреляции между количеством найденных страниц и количеством инженеров, курсов, вакансий, но не ранжирует языки по качеству или количеству написанного кода.  
Java признан языком года во второй раз, после десятилетнего перерыва, в результате роста популярности на 5,94%. Второй и третий показатели роста популярности в этом году продемонстрировали Visual Basic .NET (+ 1,51%) и Python (+ 1,24%).  
«В настоящее время Java является самым популярным языком как на рынке корпоративного бэкенда, так и в растущей сфере мобильной разработки на Android. Кроме того, Java интегрирует такие современные особенности, как лямбда-выражения и потоки. Этот язык ожидает светлое будущее», — отмечают авторы индекса.
Представители TIOBE также обращают внимание на падение популярности языка Objective-C (-5,88%), которое они связывают с намерением Apple постепенно заменить его на свой новый язык Swift. Однако симметричного роста последнего пока не наблюдается. На фоне падения PHP (-1.08%) и PL/SQL (-1.00%) TIOBE также отмечает взлёт популярности языков Groovy (с 82 на 17 место), Erlang (с 89 на 35), Haskell (с 96 на 39) и Rust (со 126 на 47).
Что до диспозиции в 2016 году, авторы прогнозируют попадание Java, PHP, JavaScript и Swift в топ-10, Scala — в топ-20, а также рост популярности Rust, Clojure, Julia и TypeScript.
Компания TIOBE выбирает «Язык года» (Programming Language of the Year), начиная с 2003. За эти годы победителями становились:
2014 Javascript
2013 Transact-SQL
2012 Objective-C
2011 Objective-C
2010 Python
2009 Go
2008 C
2007 Python
2006 Ruby
2005 Java
2004 PHP
2003 C++',to_timestamp('11-JAN-16 03.29.16.523000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('11-JAN-16','DD-MON-RR'));
Insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (70,'75 гадоў Уладзіміру Мулявіну','Сем фактаў пра Уладзіміра Мулявіна, якому 12 студзеня споўнілася б 75 гадоў','Уладзімір Мулявін нарадзіўся ў 1941 у Свярдлоўску (цяпер Екацерынбург, Расія), вучыўся ў мясцовым музычным вучылішчы, працаваў у сібірскіх філармоніях.
У 1963 запрошаны ў Белдзяржфілармонію. У 1968 заснаваў пры ёй ансамбль Лявоны, які ў 1970 стаў Песнярамі. Вывучыў беларускую мову, каб працаваць з народнай песняй. Дзякуючы Песнярам беларускі фальклор і класічная паэзія (Купала, Колас, Багдановіч, Танк) сталі вядомыя на ўвесь СССР. Сабраў залаты склад музыкаў: Місевіч, Барткевіч, Кашапараў, Тышко, Дзямешка, Дайнэка, Палівода. У 2002 Мулявін трапіў у жорсткую аўтааварыю і 26 студзеня 2003 памёр ад атрыманых траўмаў у шпіталі. Пахаваны на Усходніх могілках Мінска. Песняры застаюцца адным з найбольшых культурных дасягненняў Беларусі ў ХХ стагоддзі. Продкі Уладзіміра Мулявіна былі заможнымі сібірскімі купцамі. Яны мелі свае магазіны, былі адукаванымі. Савецкая ўлада рэпрэсавала іх. І ўжо бацька Уладзіміра, Георгій Мулявін, быў простым рабочым на заводзе Уралмаш. Ён быў музычна адораным, добра іграў на гітары. Георгій сышоў з сям''і да іншай жанчыны, пакінуўшы жонку з трыма дзецьмі. Мулявін не меў скончанай музычнай адукацыі, бо яго адлічылі з другога курса. Яго першым настаўнікам музыкі быў палітвязень Аляксандр Наўроцкі. Як расказваў Мулявін, гэты выпускнік Харкаўскага інстытута культуры і былы вязень сталінскіх лагераў быў незвычайна таленавітым музыкам-педагогам. Ён першым разглядзеў у Мулявіну не толькі талент, але і каласальную працаздольнасць. Наўроцкі займаўся са сваім выхаванцам па шэсць-сем гадзін у суткі, і ў выніку Мулявін паступіў у Свярдлоўскае музычнае вучылішча. Ён вучыўся на аддзяленні народных інструментаў, а самастойна асвоіў гітару, фартэпіяна і стварыў джаз-бэнд. Мулявін не меў скончанай музычнай адукацыі, бо яго адлічылі з другога курса за нізкапаклонства перад заходняй музыкай. Рваў уласныя ноты. Патрабавальнасць Мулявіна да сябе пераходзіла усякія межы.',to_timestamp('12-JAN-16 08.35.31.796000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('12-JAN-16','DD-MON-RR'));
Insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (0,'Title','short text content','full text',to_timestamp('01-JAN-70 03.16.40.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('14-JAN-16','DD-MON-RR'));
Insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (75,'modified title','modified short text','modified full text',to_timestamp('14-JAN-16 03.47.13.264000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('21-JAN-16','DD-MON-RR'));
Insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (79,'sf','shortText','fullText',to_timestamp('14-JAN-16 04.08.11.458000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('14-JAN-16','DD-MON-RR'));
Insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (81,'sf','shortText','fullText',to_timestamp('14-JAN-16 04.11.21.682000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('14-JAN-16','DD-MON-RR'));
Insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (93,'Title','short text content','Author test',to_timestamp('01-JAN-70 03.16.40.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('14-JAN-16','DD-MON-RR'));
Insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (94,'Title','short text content','Author test',to_timestamp('01-JAN-70 03.16.40.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('14-JAN-16','DD-MON-RR'));
Insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (95,'Title','short text content','Author test',to_timestamp('01-JAN-70 03.16.40.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('14-JAN-16','DD-MON-RR'));
Insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (96,'transaction','transaction','transaction',to_timestamp('16-JAN-16 08.20.08.693000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-JAN-16','DD-MON-RR'));
Insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (98,'transaction','transaction','transaction',to_timestamp('16-JAN-16 08.21.57.479000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-JAN-16','DD-MON-RR'));
Insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (99,'transaction','transaction','transaction',to_timestamp('16-JAN-16 08.51.56.450000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-JAN-16','DD-MON-RR'));
Insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (101,'Title','short text content','Author test',to_timestamp('01-JAN-70 03.16.40.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-JAN-16','DD-MON-RR'));
Insert into NEWS (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) values (103,'Spring','Transaction','Management',to_timestamp('21-JAN-16 03.40.33.325000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('21-JAN-16','DD-MON-RR'));
REM INSERTING into NEWS_AUTHORS
SET DEFINE OFF;
Insert into NEWS_AUTHORS (NEWS_ID,AUTHOR_ID) values (23,9);
Insert into NEWS_AUTHORS (NEWS_ID,AUTHOR_ID) values (66,9);
Insert into NEWS_AUTHORS (NEWS_ID,AUTHOR_ID) values (67,9);
Insert into NEWS_AUTHORS (NEWS_ID,AUTHOR_ID) values (68,9);
Insert into NEWS_AUTHORS (NEWS_ID,AUTHOR_ID) values (69,51);
Insert into NEWS_AUTHORS (NEWS_ID,AUTHOR_ID) values (70,51);
Insert into NEWS_AUTHORS (NEWS_ID,AUTHOR_ID) values (0,51);
Insert into NEWS_AUTHORS (NEWS_ID,AUTHOR_ID) values (0,51);
Insert into NEWS_AUTHORS (NEWS_ID,AUTHOR_ID) values (75,51);
Insert into NEWS_AUTHORS (NEWS_ID,AUTHOR_ID) values (79,51);
Insert into NEWS_AUTHORS (NEWS_ID,AUTHOR_ID) values (81,55);
Insert into NEWS_AUTHORS (NEWS_ID,AUTHOR_ID) values (93,55);
Insert into NEWS_AUTHORS (NEWS_ID,AUTHOR_ID) values (94,55);
Insert into NEWS_AUTHORS (NEWS_ID,AUTHOR_ID) values (95,55);
Insert into NEWS_AUTHORS (NEWS_ID,AUTHOR_ID) values (96,51);
Insert into NEWS_AUTHORS (NEWS_ID,AUTHOR_ID) values (98,51);
Insert into NEWS_AUTHORS (NEWS_ID,AUTHOR_ID) values (99,51);
Insert into NEWS_AUTHORS (NEWS_ID,AUTHOR_ID) values (101,55);
Insert into NEWS_AUTHORS (NEWS_ID,AUTHOR_ID) values (103,51);
REM INSERTING into NEWS_TAGS
SET DEFINE OFF;
Insert into NEWS_TAGS (NEWS_ID,TAG_ID) values (67,15);
Insert into NEWS_TAGS (NEWS_ID,TAG_ID) values (67,16);
Insert into NEWS_TAGS (NEWS_ID,TAG_ID) values (68,15);
Insert into NEWS_TAGS (NEWS_ID,TAG_ID) values (69,12);
Insert into NEWS_TAGS (NEWS_ID,TAG_ID) values (69,14);
Insert into NEWS_TAGS (NEWS_ID,TAG_ID) values (70,11);
Insert into NEWS_TAGS (NEWS_ID,TAG_ID) values (70,16);
Insert into NEWS_TAGS (NEWS_ID,TAG_ID) values (0,18);
Insert into NEWS_TAGS (NEWS_ID,TAG_ID) values (0,19);
Insert into NEWS_TAGS (NEWS_ID,TAG_ID) values (0,6);
Insert into NEWS_TAGS (NEWS_ID,TAG_ID) values (75,18);
Insert into NEWS_TAGS (NEWS_ID,TAG_ID) values (75,19);
Insert into NEWS_TAGS (NEWS_ID,TAG_ID) values (75,6);
Insert into NEWS_TAGS (NEWS_ID,TAG_ID) values (96,20);
Insert into NEWS_TAGS (NEWS_ID,TAG_ID) values (23,5);
Insert into NEWS_TAGS (NEWS_ID,TAG_ID) values (23,6);
Insert into NEWS_TAGS (NEWS_ID,TAG_ID) values (23,7);
Insert into NEWS_TAGS (NEWS_ID,TAG_ID) values (66,11);
REM INSERTING into ROLES
SET DEFINE OFF;
Insert into ROLES (ROLE_ID,ROLE_NAME) values (1,'ADMIN');
Insert into ROLES (ROLE_ID,ROLE_NAME) values (2,'USER');
REM INSERTING into TAGS
SET DEFINE OFF;
Insert into TAGS (TAG_ID,TAG_NAME) values (12,'IT');
Insert into TAGS (TAG_ID,TAG_NAME) values (2,'funny');
Insert into TAGS (TAG_ID,TAG_NAME) values (3,'like');
Insert into TAGS (TAG_ID,TAG_NAME) values (14,'java');
Insert into TAGS (TAG_ID,TAG_NAME) values (5,'France');
Insert into TAGS (TAG_ID,TAG_NAME) values (6,'Paris');
Insert into TAGS (TAG_ID,TAG_NAME) values (7,'terror');
Insert into TAGS (TAG_ID,TAG_NAME) values (15,'economics');
Insert into TAGS (TAG_ID,TAG_NAME) values (16,'Belarus');
Insert into TAGS (TAG_ID,TAG_NAME) values (20,'transaction');
Insert into TAGS (TAG_ID,TAG_NAME) values (11,'music');
Insert into TAGS (TAG_ID,TAG_NAME) values (18,'example');
Insert into TAGS (TAG_ID,TAG_NAME) values (19,'example2');
REM INSERTING into USERS
SET DEFINE OFF;
