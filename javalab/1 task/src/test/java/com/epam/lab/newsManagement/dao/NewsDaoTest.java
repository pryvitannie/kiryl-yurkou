package com.epam.lab.newsManagement.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.dbunit.annotation.ExpectedDataSet;

import com.epam.lab.newsManagement.dao.impl.JdbcAuthorDao;
import com.epam.lab.newsManagement.dao.impl.JdbcCommentDao;
import com.epam.lab.newsManagement.dao.impl.JdbcNewsDao;
import com.epam.lab.newsManagement.dao.impl.JdbcTagDao;
import com.epam.lab.newsManagement.entity.Author;
import com.epam.lab.newsManagement.entity.News;
import com.epam.lab.newsManagement.entity.Tag;

public class NewsDaoTest extends UnitilsJUnit4 {

	/*@TestDataSource
	private DataSource dataSource;
	private NewsDao newsDao;

	@Before
	public void initializeDao() {
		SqlNewsDao sqlNewsDao = new SqlNewsDao();
		sqlNewsDao.setDataSource(dataSource);
		newsDao = sqlNewsDao;
	}

	@Test
	@ExpectedDataSet
	public void testAddNews() {
		try {
			News news = new News();
			news.setCreationDate(new Timestamp(1000000));
			news.setModificationDate(new Date());
			Author author = new Author();
			author.setId(50L);
			news.setAuthor(author);
			news.setFullText("Author test");
			news.setTitle("Title");
			news.setShortText("short text content");
			news.setTags(new ArrayList<Tag>());
			newsDao.addNews(news);
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}

	@Test
	@ExpectedDataSet
	public void testEditNews() {
		try {
			News news = new News();
			news.setId(75L);
			news.setCreationDate(new Timestamp(1000000));
			news.setModificationDate(new Date());
			news.setAuthor(new Author());
			news.setFullText("modified full text");
			news.setTitle("modified title");
			news.setShortText("modified short text");
			newsDao.editNews(news);
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}*/

	/*
	 * @Test public void testNewsCount() { int newsCount = 0; try { newsCount =
	 * newsDao.getNewsCount(); } catch (DaoException e) { e.printStackTrace(); }
	 * Assert.assertEquals(27, newsCount); }
	 */
}
