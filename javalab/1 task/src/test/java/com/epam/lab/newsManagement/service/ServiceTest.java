package com.epam.lab.newsManagement.service;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.epam.lab.newsManagement.service.impl.NewsServiceImpl;

public class ServiceTest {

	@Test
	public void testAddNews() {
		NewsService test = Mockito.mock(NewsServiceImpl.class);
		try {
			Mockito.when(test.getNewsCount()).thenReturn(150);
			Assert.assertEquals((test.getNewsCount()), 150);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
}
