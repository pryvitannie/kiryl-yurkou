package com.epam.lab.newsManagement.dao;

import java.util.List;

import com.epam.lab.newsManagement.entity.News;

public interface NewsDao {

	Long addNews(News news) throws DaoException;

	void editNews(News news) throws DaoException;

	void deleteNews(Long newsId) throws DaoException;

	List<News> getAllNews() throws DaoException;

	News getNewsById(Long newsId) throws DaoException;

	List<News> getNewsByAuthorAndTags(Long authorId, List<Long> tags) throws DaoException;

	List<News> getNewsByAuthor(Long authorId) throws DaoException;

	List<News> getNewsByTags(List<Long> tagIdList) throws DaoException;

	int getNewsCount() throws DaoException;

}
