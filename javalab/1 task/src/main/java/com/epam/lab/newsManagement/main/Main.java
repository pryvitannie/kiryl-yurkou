package com.epam.lab.newsManagement.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.lab.newsManagement.service.ServiceException;
import com.epam.lab.newsManagement.service.TagService;

public class Main {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");		
		TagService tagService = (TagService) context.getBean("tagService");
		try {
			tagService.addTag(40L, "spring");
		} catch (ServiceException e) {
			
		}
	}
}
