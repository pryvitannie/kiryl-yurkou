package com.epam.lab.newsManagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.epam.lab.newsManagement.dao.DaoException;
import com.epam.lab.newsManagement.dao.UserDao;
import com.epam.lab.newsManagement.entity.User;

public class JdbcUserDao extends JdbcDao implements UserDao {

	private static final String INSERT_USER_SQL = "INSERT INTO USERS"
			+ " (USER_ID, USER_NAME, LOGIN, PASSWORD, ROLE_ID)" 
			+ " VALUES (?, ?, ?, ?, ?)";
	private static final String DELETE_NEWS_SQL = "DELETE FROM USER"
			+ " WHERE USER_ID = ?";
	private static final String GET_NEXT_USER_ID_SQL = "SELECT USER_ID_SEQ.NEXTVAL FROM dual";

	public JdbcUserDao(DataSource dataSource) {
		super(dataSource);
	}
	
	@Override
	public Long addUser(User user) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		int result;
		try {
			user.setId(getNextUserId());
			preparedStatement = connection.prepareStatement(INSERT_USER_SQL);
			preparedStatement.setLong(1, user.getId());
			preparedStatement.setString(2, user.getUsername());
			preparedStatement.setString(3, user.getLogin());
			preparedStatement.setString(4, user.getPassword());
			preparedStatement.setLong(5, user.getRoleId());
			result = preparedStatement.executeUpdate();
			if (result == 0) {
				throw new DaoException("Can't add news");
			}
		} catch (SQLException e) {
			throw new DaoException("Can't add news", e);
		} finally {
			closeResources(preparedStatement, connection);
		}
		return user.getId();
	}

	@Override
	public void deleteUser(Long userId) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		int result;
		try {
			preparedStatement = connection.prepareStatement(DELETE_NEWS_SQL);
			preparedStatement.setLong(1, userId);
			result = preparedStatement.executeUpdate();
			if (result == 0) {
				throw new DaoException("There is no user having id = " + userId);
			}
		} catch (SQLException e) {
			throw new DaoException("Can't delete user", e);
		} finally {
			closeResources(preparedStatement, connection);
		}

	}

	private Long getNextUserId() throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = connection.prepareStatement(GET_NEXT_USER_ID_SQL);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			return resultSet.getLong(1);
		} catch (SQLException e) {
			throw new DaoException("Can't get next news id", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
	}

}
