package com.epam.lab.newsManagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.lab.newsManagement.dao.DaoException;
import com.epam.lab.newsManagement.dao.TagDao;
import com.epam.lab.newsManagement.entity.Tag;

public class JdbcTagDao extends JdbcDao implements TagDao {

	private static final String SELECT_TAG_SQL = "SELECT TAG_ID, TAG_NAME" 
			+ " FROM TAGS" 
			+ " WHERE TAG_NAME = ?";
	private static final String INSERT_TAG_SQL = "INSERT INTO TAGS" 
			+ " (TAG_ID, TAG_NAME)" 
			+ " VALUES (?, ?)";
	private static final String INSERT_TAG_TO_NEWS_SQL = "INSERT INTO NEWS_TAGS"
			+ " (NEWS_ID, TAG_ID)"
			+ " VALUES (?, ?)";
	private static final String SELECT_COMMENTS_BY_NEWS_SQL = "SELECT TAGS.TAG_ID, TAGS.TAG_NAME" 
			+ " FROM TAGS"
			+ " JOIN NEWS_TAGS ON TAGS.TAG_ID = NEWS_TAGS.TAG_ID" 
			+ " WHERE NEWS_TAGS.NEWS_ID = ?";
	private static final String GET_NEXT_TAG_ID_SQL = "SELECT TAG_ID_SEQ.NEXTVAL FROM dual";

	public JdbcTagDao(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	public boolean tagExists(Tag tag) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		boolean result;
		try {
			preparedStatement = connection.prepareStatement(SELECT_TAG_SQL);
			preparedStatement.setString(1, tag.getName());
			resultSet = preparedStatement.executeQuery();
			result = resultSet.next();
			if (result) {
				tag.setId(resultSet.getLong(1));
			}
		} catch (SQLException e) {
			throw new DaoException("Can't get tag", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
		return result;
	}

	@Override
	public void addTagToNews(Long newsId, Long tagId) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(INSERT_TAG_TO_NEWS_SQL);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, tagId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			closeResources(preparedStatement, connection);
		}
	}

	@Override
	public Long addNewTag(Tag tag) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		try {
			tag.setId(getNextTagId());
			preparedStatement = connection.prepareStatement(INSERT_TAG_SQL);
			preparedStatement.setLong(1, tag.getId());
			preparedStatement.setString(2, tag.getName());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			closeResources(preparedStatement, connection);
		}
		return tag.getId();
	}

	@Override
	public List<Tag> getTagsByNews(Long newsId) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Tag> tags = new ArrayList<>();
		Tag tag;
		try {
			preparedStatement = connection.prepareStatement(SELECT_COMMENTS_BY_NEWS_SQL);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				tag = new Tag();
				tag.setId(resultSet.getLong("TAG_ID"));
				tag.setName(resultSet.getString("TAG_NAME"));
				tags.add(tag);
			}
		} catch (SQLException e) {
			throw new DaoException("Can't set tags to news", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
		return tags;
	}

	private Long getNextTagId() throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = connection.prepareStatement(GET_NEXT_TAG_ID_SQL);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			return resultSet.getLong(1);
		} catch (SQLException e) {
			throw new DaoException("Can't get next tag id", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
	}

}
