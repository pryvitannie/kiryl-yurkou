package com.epam.lab.newsManagement.service;

import java.util.List;

import com.epam.lab.newsManagement.entity.Tag;

public interface TagService {

	boolean addTag(Long newsId, String tagName) throws ServiceException;

	List<Tag> getTagsByNews(Long newsId) throws ServiceException;

}
