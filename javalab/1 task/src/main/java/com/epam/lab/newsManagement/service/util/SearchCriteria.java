package com.epam.lab.newsManagement.service.util;

import java.util.ArrayList;
import java.util.List;

public class SearchCriteria {

	private Long authorId = null;
	private List<Long> tagIdList = null;

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}

	public List<Long> getTagIdList() {
		return tagIdList;
	}

	public void addTagId(long tagId) {
		if (tagIdList == null) {
			tagIdList = new ArrayList<>();
		}
		tagIdList.add(tagId);
	}

}
