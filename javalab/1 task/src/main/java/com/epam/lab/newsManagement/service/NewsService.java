package com.epam.lab.newsManagement.service;

import java.util.List;

import com.epam.lab.newsManagement.entity.News;
import com.epam.lab.newsManagement.entity.Tag;
import com.epam.lab.newsManagement.service.util.SearchCriteria;

public interface NewsService {

	boolean addNews(String title, String shortText, String fullText, long authorId, List<Tag> tags)
			throws ServiceException;

	boolean editNews(long newsId, String title, String shortText, String fullText) throws ServiceException;

	boolean deleteNews(long newsId) throws ServiceException;

	List<News> getAllNews() throws ServiceException;

	News getNewsById(long newsId) throws ServiceException;

	List<News> search(SearchCriteria searchCriteria) throws ServiceException;

	int getNewsCount() throws ServiceException;

}
