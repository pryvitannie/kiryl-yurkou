package com.epam.lab.newsManagement.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.lab.newsManagement.dao.DaoException;
import com.epam.lab.newsManagement.dao.NewsDao;
import com.epam.lab.newsManagement.entity.News;
import com.epam.lab.newsManagement.entity.Tag;
import com.epam.lab.newsManagement.service.AuthorService;
import com.epam.lab.newsManagement.service.CommentService;
import com.epam.lab.newsManagement.service.NewsService;
import com.epam.lab.newsManagement.service.ServiceException;
import com.epam.lab.newsManagement.service.TagService;
import com.epam.lab.newsManagement.service.util.SearchCriteria;

public class NewsServiceImpl implements NewsService {

	private final static Logger LOGGER = LogManager.getRootLogger();

	private AuthorService authorService;
	private TagService tagService;
	private NewsDao newsDao;
	private CommentService commentService;

	@Override
	@Transactional(rollbackFor = ServiceException.class)
	public boolean addNews(String title, String shortText, String fullText, long authorId, List<Tag> tags)
			throws ServiceException {
		Calendar calendar = new GregorianCalendar();
		Date currentDate = calendar.getTime();
		News news = new News();
		Long newsId;
		news.setTitle(title);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setCreationDate(currentDate);
		news.setModificationDate(currentDate);
		try {
			newsId = newsDao.addNews(news);
			authorService.addAuthorToNews(newsId, authorId);
			for (Tag tag : tags) {
				tagService.addTag(newsId, tag.getName());
			}
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in NewsService::addNews, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("News has been added");
		return true;
	}

	@Override
	public boolean editNews(long newsId, String title, String shortText, String fullText) throws ServiceException {
		Calendar calendar = new GregorianCalendar();
		Date currentDate = calendar.getTime();
		News news = new News();
		news.setTitle(title);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setModificationDate(currentDate);
		news.setId(newsId);
		try {
			newsDao.editNews(news);
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in NewsService::editNews, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("News has been edited");
		return true;
	}

	@Override
	public boolean deleteNews(long newsId) throws ServiceException {
		try {
			newsDao.deleteNews(newsId);
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in NewsService::deleteNews, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("News having id = " + newsId + " has been deleted");
		return true;
	}

	@Override
	@Transactional(rollbackFor = ServiceException.class)
	public List<News> getAllNews() throws ServiceException {
		List<News> newsList = null;
		try {
			newsList = newsDao.getAllNews();
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in NewsService::getAllNews, full stack trace follows", e);
			throw new ServiceException(e);
		}
		for (News news : newsList) {
			news.setAuthor(authorService.getAuthorByNews(news.getId()));
			news.setTags(tagService.getTagsByNews(news.getId()));
			news.setComments(commentService.getCommentsByNews(news.getId()));
		}
		LOGGER.debug("News list has been successfully received");
		return newsList;
	}

	@Override
	public News getNewsById(long newsId) throws ServiceException {
		News news;
		try {
			news = newsDao.getNewsById(newsId);
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in NewsService::getNewsById, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("News having id = " + newsId + " has been received");
		return news;
	}

	@Override
	public List<News> search(SearchCriteria searchCriteria) throws ServiceException {
		List<News> newsList = new ArrayList<>();
		try {
			if (searchCriteria.getAuthorId() != null) {
				if (searchCriteria.getTagIdList() != null) {
					newsList = newsDao.getNewsByAuthorAndTags(searchCriteria.getAuthorId(),
							searchCriteria.getTagIdList());
				} else {
					newsList = newsDao.getNewsByAuthor(searchCriteria.getAuthorId());
				}
			} else if (searchCriteria.getTagIdList() != null) {
				newsList = newsDao.getNewsByTags(searchCriteria.getTagIdList());
			}
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in NewsService::search, full stack trace follows", e);
			throw new ServiceException(e);
		}
		for (News news : newsList) {
			news.setAuthor(authorService.getAuthorByNews(news.getId()));
			news.setTags(tagService.getTagsByNews(news.getId()));
			news.setComments(commentService.getCommentsByNews(news.getId()));
		}
		LOGGER.debug("News has been found by search criteria");
		return newsList;
	}

	@Override
	public int getNewsCount() throws ServiceException {
		int count = 0;
		try {
			count = newsDao.getNewsCount();
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in NewsService::getNewsCount, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("There are " + count + " news");
		return count;
	}

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	public void setNewsDao(NewsDao newsDao) {
		this.newsDao = newsDao;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

}
