package com.epam.lab.newsManagement.dao;

import java.util.List;

import com.epam.lab.newsManagement.entity.Comment;

public interface CommentDao {

    Long addComment(Comment comment) throws DaoException;

    void deleteComment(Long commentId) throws DaoException;

    List<Comment> getCommentsByNews(Long newsId) throws DaoException;

}
