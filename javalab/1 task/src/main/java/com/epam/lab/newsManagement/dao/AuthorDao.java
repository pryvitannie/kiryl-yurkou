package com.epam.lab.newsManagement.dao;

import java.util.List;

import com.epam.lab.newsManagement.entity.Author;

public interface AuthorDao {

	Long addAuthor(Author author) throws DaoException;

	void addAuthorToNews(Long newsId, Long authorId) throws DaoException;

	List<Author> getAllAuthors() throws DaoException;

	Author getAuthorByNews(Long newsId) throws DaoException;

	void expireAuthor(Author author) throws DaoException;

	void deleteAuthor(Long authorId) throws DaoException;

}
