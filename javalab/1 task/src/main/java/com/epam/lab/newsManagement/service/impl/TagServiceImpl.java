package com.epam.lab.newsManagement.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.lab.newsManagement.dao.DaoException;
import com.epam.lab.newsManagement.dao.TagDao;
import com.epam.lab.newsManagement.entity.Tag;
import com.epam.lab.newsManagement.service.ServiceException;
import com.epam.lab.newsManagement.service.TagService;

public class TagServiceImpl implements TagService {

	private final static Logger LOGGER = LogManager.getRootLogger();

	private TagDao tagDao;

	@Override
	@Transactional (rollbackFor = ServiceException.class)
	public boolean addTag(Long newsId, String tagName) throws ServiceException {
		Tag tag = new Tag();
		tag.setName(tagName);
		try {
			if (!tagDao.tagExists(tag)) {
				tagDao.addNewTag(tag);
			}
			tagDao.addTagToNews(newsId, tag.getId());
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in TagService::addTag, full stack trace follows", e);
			throw new ServiceException(e);
		}
		return true;
	}

	@Override
	public List<Tag> getTagsByNews(Long newsId) throws ServiceException {
		List<Tag> tags = null;
		try {
			tagDao.getTagsByNews(newsId);
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in TagService::getTagsByNews, full stack trace follows", e);
			throw new ServiceException(e);
		}
		return tags;
	}

	public void setTagDao(TagDao tagDao) {
		this.tagDao = tagDao;
	}

}
