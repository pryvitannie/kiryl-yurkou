package com.epam.lab.newsManagement.dao;

import java.util.List;

import com.epam.lab.newsManagement.entity.Tag;

public interface TagDao {

	List<Tag> getTagsByNews(Long newsId) throws DaoException;

	boolean tagExists(Tag tag) throws DaoException;
	
	void addTagToNews(Long newsId, Long tagId) throws DaoException;
	
	Long addNewTag(Tag tag) throws DaoException;
	
}
