<%@ page contentType="text/html;charset=UTF-8" language="java"
  pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>News portal</title>
<c:url var="rootUrl" value="/" />
<link
  href='https://fonts.googleapis.com/css?family=Lobster&subset=latin,cyrillic'
  rel='stylesheet' type='text/css'>
<link
  href='https://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic-ext'
  rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css"
  href="${rootUrl}resources/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
  href="${rootUrl}resources/css/style.css">
</head>
<body>
  <div class="container-fluid">
    <div class="searchCriterias">
      <form action="${rootUrl}news">
        <div class="form-group">
          <label for="selectTags">Tags</label> <select id="selectTags"
            name="tagId" multiple="multiple" class="form-control">
            <c:forEach var="tag" items="${tags}">
              <option value="${tag.id}"><c:out
                  value="${tag.name}" /></option>
            </c:forEach>
          </select>
        </div>
        <div class="form-group">
          <label for="selectAuthor">Author</label> <select id="selectAuthor"
            name="authorId" class="form-control">
            <option style="text-color: #eee;" selected disabled value=''>Author</option>
            <c:forEach var="author" items="${authors}">
              <option value="${author.id}"><c:out
                  value="${author.name}" /></option>
            </c:forEach>
          </select>
        </div>
        <button type="submit" class="btn btn-default">Search</button>
      </form>
    </div>
    <div class="newsList">
      <c:forEach var="news" items="${newsList}">
        <div class="news">

          <div class="row">
            <div class="col-md-7">
              <c:url var="getNewsUrl" value="/news/${news.id}" />
              <a href="${getNewsUrl}"><strong><span
                  class="title"><c:out value="${news.title}" /></span></strong></a>
            </div>
            <div class="col-md-2 text-right">
              (by
              <c:out value="${news.author.name}" />
              )
            </div>
            <div class="col-md-2">
              <c:out value="${news.modificationDate}" />
            </div>
            <div class="col-md-1">
              <form action="${rootUrl}news/${news.id}/delete" method="post">
                <button type="submit" class="btn btn-default">
                  <span class="glyphicon glyphicon-remove"> </span>
                </button>
              </form>
            </div>
          </div>
          <div class="shortText">
            <div class="row">
              <div class="col-md-12">
                <c:out value="${news.shortText}" />
              </div>
            </div>
          </div>
          <div class="info">
            <div class="row">
              <div class="col-md-4 col-md-offset-6 text-right">
                <div class="tags">
                  <c:forEach var="tag" items="${news.tags}">
                    <span class="tag"><c:out value="${tag.name}" /></span>
                  </c:forEach>
                </div>
              </div>
              <div class="col-md-2">
                Comments(
                <c:out value="${news.comments.size()}" />
                )
              </div>
            </div>
          </div>
        </div>
      </c:forEach>
      <div class="row">
        <div class="text-center">
          <nav>
          <ul class="pagination">
            <li><c:url var="getNewsPageUrl" value="/news">
                <c:param name="page" value="${page - 1}" />
              </c:url> <a href="${getNewsPageUrl}" aria-label="Previous"> <span
                aria-hidden="true">&laquo;</span>
            </a></li>
            <c:forEach var="i" begin="${beginPageNumber}"
              end="${endPageNumber}">
              <c:url var="getNewsPageUrl" value="/news">
                <c:param name="page" value="${i}" />
              </c:url>
              <c:choose>
                <c:when test="${i == page}">
                  <li class="active"><a href="${getNewsPageUrl}"><c:out
                        value="${i}" /></a></li>
                </c:when>
                <c:otherwise>
                  <li><a href="${getNewsPageUrl}"><c:out
                        value="${i}" /></a></li>
                </c:otherwise>
              </c:choose>
            </c:forEach>
            <li><c:url var="getNewsPageUrl" value="/news">
                <c:param name="page" value="${page + 1}" />
              </c:url> <a href="${getNewsPageUrl}" aria-label="Next"> <span
                aria-hidden="true">&raquo;</span>
            </a></li>
          </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
</body>
</html>