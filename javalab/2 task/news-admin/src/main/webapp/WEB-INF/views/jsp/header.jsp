<%@ page contentType="text/html;charset=UTF-8" language="java"
  pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>News portal</title>
<c:url var="rootUrl" value="/" />
<link
  href='https://fonts.googleapis.com/css?family=Lobster&subset=latin,cyrillic'
  rel='stylesheet' type='text/css'>
<link
  href='https://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic-ext'
  rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css"
  href="${rootUrl}resources/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="${rootUrl}resources/css/style.css">
</head>
<body>
  <div class="page-header">
    <h1 class="text-center">
      <a href="${rootUrl}news">News portal</a>      
    </h1>
    <a href="<c:url value="/j_spring_security_logout" />" >Logout</a>
    <c:url var="enUrl" value="${requestScope['javax.servlet.forward.request_url']}">
      <c:param name="lang" value="en"/>
    </c:url>
    <c:url var="belUrl" value="${requestScope['javax.servlet.forward.request_url']}">
      <c:param name="lang" value="bel"/>
    </c:url>
    <c:url var="ruUrl" value="${requestScope['javax.servlet.forward.request_url']}">
      <c:param name="lang" value="ru"/>
    </c:url>
    <a href="${enUrl}"/>en</a>
    <a href="${belUrl}">bel</a>
    <a href="${ruUrl}">ru</a>
    <spring:message code="welcome" text="Employee Id" />
  </div>
</body>
</html>