<%@ page contentType="text/html;charset=UTF-8" language="java"
  pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<c:url var="rootUrl" value="/" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>News portal</title>
<link
  href='https://fonts.googleapis.com/css?family=Lobster&subset=latin,cyrillic'
  rel='stylesheet' type='text/css'>
<link
  href='https://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic-ext'
  rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css"
  href="${rootUrl}/resources/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
  href="${rootUrl}/resources/css/style.css">
</head>
<body>
  <div class="container-fluid">
    <form class="form-horizontal" action="${rootUrl}news" method="post" accept-charset="UTF-8">
      <input type="hidden" name="newsId" value="${news.id}">
      <input type="hidden" name="isNew" value="${isNew}">
      <div class="form-group">
        <label for="inputTitle" class="col-md-2 control-label">Title</label>
        <div class="col-md-8">
          <input type="text" class="form-control" name="title" 
          value=
          "<c:if test="${not empty news}"><c:out
              value="${news.title}"></c:out></c:if>"
            id="inputTitle" required="required" pattern="(\w|\s)+">
        </div>
      </div>
      <div class="form-group">
        <label for="inputTitle" class="col-md-2 control-label">Date</label>
        <div class="col-md-8">
          <input type="date"  class="form-control" name="date" 
          value=
          "<c:choose><c:when test="${not empty news}"><fmt:formatDate pattern="yyyy-MM-dd"
            value="${news.modificationDate}" /></c:when><c:otherwise><fmt:formatDate pattern="yyyy-MM-dd"
            value="${currentDate}" /></c:otherwise></c:choose>"
            id="inputDate">
        </div>
      </div>
      <div class="form-group">
        <label for="inputShortText" class="col-md-2 control-label">Short
          text</label>
        <div class="col-md-8">
          <textarea class="form-control" name="shortText"
            id="inputShortText" required="required" maxlength="100"><c:if test="${not empty news}"><c:out
              value="${news.shortText}"></c:out></c:if></textarea>
        </div>
      </div>
      <div class="form-group">
        <label for="inputFullText" class="col-md-2 control-label">Full
          text</label>
        <div class="col-md-8">
          <textarea class="form-control" name="fullText"
            id="inputFullText" rows="12" required="required" maxlength="300"><c:if test="${not empty news}"><c:out
              value="${news.fullText}"></c:out></c:if></textarea>
        </div>
      </div>
      <div class="form-group">
        <label for="selectAuthor" class="col-md-2 control-label">Author</label>
        <div class="col-md-8">
          <select class="form-control" id="selectAuthor" name="authorId" required="required">
            <option value="" disabled selected>Author</option>
            <c:forEach var="author" items="${authors}">
              <option value="${author.id}">
                <c:out value="${author.name}" />
              </option>
            </c:forEach>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="selectTags" class="col-md-2 control-label">Tags</label>
        <div class="col-md-8">
          <select class="form-control" id="selectTags" name="tagIdList"
            multiple="multiple">
            <c:forEach var="tag" items="${tags}">
              <option value="${tag.id}">
                <c:out value="${tag.name}" />
              </option>
            </c:forEach>
          </select>
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-offset-2 col-md-8">
          <button type="submit" class="btn btn-default">Save</button>
        </div>
      </div>
    </form>
  </div>
</body>
</html>