<%@ page contentType="text/html;charset=UTF-8" language="java"
  pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>News portal</title>
<c:url var="rootUrl" value="/" />
<link
  href='https://fonts.googleapis.com/css?family=Lobster&subset=latin,cyrillic'
  rel='stylesheet' type='text/css'>
<link
  href='https://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic-ext'
  rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css"
  href="${rootUrl}resources/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
  href="${rootUrl}resources/css/style.css">
</head>
<body>
  <div class="container-fluid">
    <div class="newsList">
      <div class="news">
        <div class="row">
          <div class="col-md-7">
            <form action="${rootUrl}tags/" method="post">
              <input type="hidden" name="isNew" value="true">
                <input type="text" name="name" value="">
                <button type="submit" class="btn btn-default">Save</button>
            </form>
          </div>
        </div>
      </div>
      <c:forEach var="tag" items="${tags}">
        <div class="news">
          <div class="row">
            <div class="col-md-7">
              <form action="${rootUrl}tags/" method="post">
                <input type="hidden" name="isNew" value="false">
                <input type="hidden" name="tagId" value="${tag.id}">
                <input type="text" name="name" value="${tag.name}">
                <button type="submit" class="btn btn-default">Save</button>
              </form>
            </div>
            <div class="col-md-1">
              <form action="${rootUrl}tags/${tag.id}/delete" method="post">
                <button type="submit" class="btn btn-default">
                  <span class="glyphicon glyphicon-remove"> </span>
                </button>
              </form>
            </div>
          </div>
        </div>
      </c:forEach>      
    </div>
  </div>
</body>
</html>