<%@ page contentType="text/html;charset=UTF-8" language="java"
  pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>News portal</title>
<c:url var="rootUrl" value="/" />
<link
  href='https://fonts.googleapis.com/css?family=Lobster&subset=latin,cyrillic'
  rel='stylesheet' type='text/css'>
<link
  href='https://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic-ext'
  rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css"
  href="${rootUrl}resources/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="${rootUrl}resources/css/style.css">
</head>
<body> 
  <div class="container-fluid">
    <div class="news">  
      <div class="row">
        <div class="col-md-2 text-center">
          <a href="${rootUrl}news/${news.id}/update">Edit news</a>
        </div>      
      </div>
      <div class="row">
        <div class="col-md-8">
          <strong><span class="title"><c:out
                value="${news.title}" /></span></strong>
        </div>
        <div class="col-md-2 text-right">
          (by
          <c:out value="${news.author.name}" />
          )
        </div>
        <div class="col-md-2">
          <c:out value="${news.modificationDate}" />
        </div>
      </div>      
      <div class="shortText">
        <div class="row">
          <div class="col-md-12">
            <c:out value="${news.fullText}" />
          </div>
        </div>
      </div>    
      <div class="info">
        <div class="row">
          <div class="col-md-4 col-md-offset-6 text-right">
            <div class="tags">
              <c:forEach var="tag" items="${news.tags}">
                <span class="tag"><c:out value="${tag.name}" /></span>
              </c:forEach>
            </div>
          </div>
          <div class="col-md-2">
            Comments(
            <c:out value="${news.comments.size()}" />
            )
          </div>
        </div>
      </div>     
      <div class="comments">
        <c:forEach var="comment" items="${news.comments}">
          <div class="row">
            <div class="comment">
              <div class="col-md-11">
                <c:out value="${comment.text}" />
              </div>
              <div class="col-md-1">
                <form action="${rootUrl}/news/${news.id}/comment/${comment.id}/delete" method="post">
                  <button  type="submit" class="btn btn-default">
                    <span class="glyphicon glyphicon-remove">
                    </span>
                  </button>
                </form> 
              </div>
            </div>            
          </div>
        </c:forEach>
        <form action="${news.id}/comment" method="post">
          <textarea class="form-control"
            placeholder="Enter comment here..." rows="4" cols="8"
            name="commentText" id="comment"></textarea>
          <button type="submit"
            class="btn btn-default">Leave a comment</button>
        </form>
      </div>    
    </div>
  </div>
</body>
</html>