package com.epam.lab.newsManagement.controller;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.lab.newsManagement.service.CommentService;
import com.epam.lab.newsManagement.service.ServiceException;

@Controller
@RequestMapping("/news/{newsId}/comment")
public class CommentController implements ApplicationContextAware {

	private ApplicationContext applicationContex;
	
	@RequestMapping(method = RequestMethod.POST)
	public String addComment(@PathVariable(value = "newsId") Long newsId,
			@RequestParam(value = "commentText") String text, ModelMap model) {
		CommentService commentService = (CommentService) applicationContex.getBean("commentService");
		try {
			commentService.addComment(text, newsId);
		} catch (ServiceException e) {
			model.addAttribute("message", e.getCause().getMessage());
			return "error";
		}
		return "redirect:/news/" + newsId;
	}
	
	@RequestMapping(value = "/{commentId}/delete", method = RequestMethod.POST)
	public String deleteComment(@PathVariable(value = "newsId") Long newsId, 
			@PathVariable(value = "commentId") Long commentId, ModelMap model) {
		CommentService commentService = (CommentService) applicationContex.getBean("commentService");
		try {
			commentService.deleteComment(commentId);
		} catch (ServiceException e) {
			model.addAttribute("message", e.getCause().getMessage());
			return "error";
		}
		return "redirect:/news/" + newsId;
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContex = applicationContext;
	}

}
