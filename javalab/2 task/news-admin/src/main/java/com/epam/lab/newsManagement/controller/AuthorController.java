package com.epam.lab.newsManagement.controller;

import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.lab.newsManagement.entity.Author;
import com.epam.lab.newsManagement.service.AuthorService;
import com.epam.lab.newsManagement.service.ServiceException;

@Controller
@RequestMapping("/authors")
public class AuthorController implements ApplicationContextAware {

	/*
		/authors				GET		Listing, display all authors, display add and update author form
		/authors				POST	Save or update author
		/authors/{id}/expire	POST	Expire author {id}
	 */
	
	private ApplicationContext applicationContext;
	
	@RequestMapping(method = RequestMethod.GET)
	public String getAuthors(ModelMap model) {
		AuthorService authorService = (AuthorService) applicationContext.getBean("authorService");
		List<Author> authors;
		try {
			authors = authorService.getAllAuthors();
			model.addAttribute("authors", authors);
		} catch (ServiceException e) {
			model.addAttribute("message", e.getCause().getMessage());
			return "error";
		}
		return "authors";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String saveOrUpdateAuthor(@RequestParam("isNew") boolean isNew,
			@RequestParam(value = "authorId", required = false) Long authorId,
			@RequestParam("name") String name, ModelMap model) {
		AuthorService authorService = (AuthorService) applicationContext.getBean("authorService");
		try {
			if (isNew) {
				authorService.addAuthor(name);
			} else {
				authorService.editAuthor(authorId, name);
			}
		} catch (ServiceException e) {
			model.addAttribute("message", e.getCause().getMessage());
			return "error";
		}
		return "redirect:/authors";
	}
	
	@RequestMapping(value = "/{id}/expire", method = RequestMethod.POST)
	public String setAuthorExpired(@PathVariable("id") Long authorId, ModelMap model) {
		AuthorService authorService = (AuthorService) applicationContext.getBean("authorService");
		try {
			authorService.setAuthorExpired(authorId);
		} catch (ServiceException e) {
			model.addAttribute("message", e.getCause().getMessage());
			return "error";
		}
		return "redirect:/authors";
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
	
}
