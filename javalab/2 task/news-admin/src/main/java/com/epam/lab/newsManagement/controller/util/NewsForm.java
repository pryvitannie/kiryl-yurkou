package com.epam.lab.newsManagement.controller.util;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

public class NewsForm {

	@NotNull
	@Size(max = 30)
	private String title;
	
	@NotNull
	@Size(max = 100)
	private String shortText;
	
	@NotNull
	@Size(max = 2000)
	private String fullText;
	
	@NotNull
	@Past
	private Date date;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
}
