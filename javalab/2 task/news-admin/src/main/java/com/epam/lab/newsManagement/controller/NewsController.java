package com.epam.lab.newsManagement.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.lab.newsManagement.entity.Author;
import com.epam.lab.newsManagement.entity.News;
import com.epam.lab.newsManagement.entity.Tag;
import com.epam.lab.newsManagement.service.AuthorService;
import com.epam.lab.newsManagement.service.NewsService;
import com.epam.lab.newsManagement.service.ServiceException;
import com.epam.lab.newsManagement.service.TagService;
import com.epam.lab.newsManagement.service.util.SearchCriteria;
import com.epam.lab.newsManagement.webutil.PaginationHelper;
import com.epam.lab.newsManagement.webutil.VisiblePageBorders;

@Controller
@RequestMapping("/news")
@SessionAttributes({"searchCriteria"})
public class NewsController implements ApplicationContextAware {

	/*
		/news					GET		Listing, display all news
		/news					POST	Save or update news
		/news/{id}				GET		Display news {id}
		/news/add				GET		Display add news form
		/news/{id}/update		GET		Display update news form for {id}
		/news/{id}/delete		POST	Delete news {id}
	 */
	
	private ApplicationContext applicationContext;
	
	@RequestMapping(method = RequestMethod.GET)
	public String getNewsList(@RequestParam(value = "page", required = false) Integer currentPageNumber,
			@RequestParam(value = "tagId", required = false) List<Long> tagIdList,
			@RequestParam(value = "authorId", required = false) Long authorId,
			ModelMap model) {	
		NewsService newsService = (NewsService) applicationContext.getBean("newsService");
		AuthorService authorService = (AuthorService) applicationContext.getBean("authorService");
		TagService tagService = (TagService) applicationContext.getBean("tagService");
		List<News> newsList = null;
		List<Tag> tags = null;
		List<Author> authors = null;
		SearchCriteria searchCriteria;
		VisiblePageBorders visiblePageBorders;
		PaginationHelper paginationHelper = (PaginationHelper) applicationContext.getBean("paginationHelper");
		if (paginationHelper == null) {
			paginationHelper = (PaginationHelper) applicationContext.getBean("paginationHelper");
			model.addAttribute("paginationHelper", paginationHelper);
		}
		if (authorId != null || tagIdList != null || currentPageNumber == null) {
			searchCriteria = new SearchCriteria();
			searchCriteria.setAuthorId(authorId);
			searchCriteria.setTagIdList(tagIdList);
			model.addAttribute("searchCriteria", searchCriteria);
		} else {
			searchCriteria = (SearchCriteria) model.get("searchCriteria");
		}	
		if (currentPageNumber == null) {
			currentPageNumber = paginationHelper.getFirstPage();
		}
		try {
			paginationHelper.initizialize(newsService.getNewsCount(searchCriteria));
			currentPageNumber = paginationHelper.correctPageNumber(currentPageNumber);
			newsList = newsService.search(searchCriteria, 
					paginationHelper.getBeginRow(currentPageNumber), paginationHelper.getEndRow(currentPageNumber));
			authors = authorService.getAllAuthors();	
			tags = tagService.getAllTags();			
		} catch (ServiceException e) {
			model.addAttribute("message", e.getCause().getMessage());
			return "error";
		}				
		visiblePageBorders = paginationHelper.calculateVisiblePageNumberBorders(currentPageNumber);
		model.addAttribute("beginPageNumber", visiblePageBorders.getBeginPageNumber());
		model.addAttribute("endPageNumber", visiblePageBorders.getEndPageNumber());
		model.addAttribute("page", currentPageNumber);
		model.addAttribute("newsList", newsList);
		model.addAttribute("authors", authors);	
		model.addAttribute("tags", tags);
		return "newsList";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String saveOrUpdateNews(@RequestParam("isNew") boolean isNew, 
			@RequestParam("newsId") Long newsId, @RequestParam("title") String title,
			@RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date,
			@RequestParam("shortText") String shortText, @RequestParam("fullText") String fullText,
			@RequestParam("authorId") Long authorId, 
			@RequestParam(value = "tagIdList", required = false) List<Long> tagIdList,
			ModelMap model)
	{
		NewsService newsService = (NewsService) applicationContext.getBean("newsService");
		System.out.println(date);
		try {
			if (isNew) {
				newsService.addNews(title, shortText, fullText, authorId, tagIdList, date);
			} else {
				newsService.editNews(newsId, title, shortText, fullText, authorId, tagIdList, date);
			}
		} catch (ServiceException e) {
			model.addAttribute("message", e.getCause().getMessage());
			return "error";
		}
		return "redirect:/news";
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String getNewsById(@PathVariable(value="id") final Long newsId, ModelMap model) {
		NewsService newsService = (NewsService) applicationContext.getBean("newsService");
		News news = null;
		try {
			news = newsService.getNewsById(newsId);
			model.addAttribute("news", news);
		} catch (ServiceException e) {
			model.addAttribute("message", e.getCause().getMessage());
			return "error";
		}		
		return "news";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String getAddNewsForm(ModelMap model) {
		AuthorService authorService = (AuthorService) applicationContext.getBean("authorService");
		TagService tagService = (TagService) applicationContext.getBean("tagService");
		List<Tag> tags = null;
		List<Author> authors = null;	
		Date date = new Date();
		System.out.println(date);
		boolean isNew = true;
		try {
			tags = tagService.getAllTags();
			authors = authorService.getAllAuthors();
			model.addAttribute("isNew", isNew);
			model.addAttribute("tags", tags);
			model.addAttribute("authors", authors);
			
			model.addAttribute("currentDate", date);
		} catch (ServiceException e) {
			model.addAttribute("message", e.getCause().getMessage());
			return "error";
		}	
		return "saveNews";
	}
	
	@RequestMapping(value = "/{id}/update", method = RequestMethod.GET)
	public String getUpdateNewsForm(@PathVariable(value = "id") final Long newsId, ModelMap model) {
		NewsService newsService = (NewsService) applicationContext.getBean("newsService");
		AuthorService authorService = (AuthorService) applicationContext.getBean("authorService");
		TagService tagService = (TagService) applicationContext.getBean("tagService");
		List<Tag> tags = null;
		List<Author> authors = null;
		News news = null;
		boolean isNew = false;
		try {
			news = newsService.getNewsById(newsId);
			tags = tagService.getAllTags();
			authors = authorService.getAllAuthors();
			model.addAttribute("news", news);		
			model.addAttribute("isNew", isNew);
			model.addAttribute("tags", tags);
			model.addAttribute("authors", authors);
		} catch (ServiceException e) {
			model.addAttribute("message", e.getCause().getMessage());
			return "error";
		}	
		return "saveNews";
	}
	
	@RequestMapping(value = "/{newsId}/delete", method = RequestMethod.POST)
	public String deleteNews(@PathVariable(value = "newsId") Long newsId,ModelMap model) {
		NewsService newsService = (NewsService) applicationContext.getBean("newsService");
		try {
			newsService.deleteNews(newsId);
		} catch (ServiceException e) {
			model.addAttribute("message", e.getCause().getMessage());
			return "error";
		}
		return "redirect:/news";
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

}
