package com.epam.lab.newsManagement.controller;

import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.lab.newsManagement.entity.Tag;
import com.epam.lab.newsManagement.service.ServiceException;
import com.epam.lab.newsManagement.service.TagService;

@Controller
@RequestMapping("/tags")
public class TagController implements ApplicationContextAware {
	
	/*
		/tags					GET		Listing, display all tags, display add and update tag form
		/tags					POST	Save or update tag
		/tags/{id}/delete		POST	Delete tag {id}
	 */
	
	private ApplicationContext applicationContext;
	
	@RequestMapping(method = RequestMethod.GET)
	public String getTags(ModelMap model) {
		TagService tagService = (TagService) applicationContext.getBean("tagService");
		List<Tag> tags;
		try {
			tags = tagService.getAllTags();
			model.addAttribute("tags", tags);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return "tags";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String saveOrUpdateTag(@RequestParam("isNew") boolean isNew,
			@RequestParam(value = "tagId", required = false) Long tagId,
			@RequestParam("name") String name, ModelMap model) {
		TagService tagService = (TagService) applicationContext.getBean("tagService");
		try {
			if (isNew) {
				tagService.addTag(name);
			} else {
				tagService.editTag(tagId, name);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return "redirect:/tags";
	}
	
	@RequestMapping(value = "/{id}/delete", method = RequestMethod.POST)
	public String deleteTag(@PathVariable("id") Long tagId) {
		TagService tagService = (TagService) applicationContext.getBean("tagService");
		try {
			tagService.deleteTag(tagId);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return "redirect:/tags";
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
	
}
