<%@ page contentType="text/html;charset=UTF-8" language="java"
  pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>News portal</title>
<link
  href='https://fonts.googleapis.com/css?family=Lobster&subset=latin,cyrillic'
  rel='stylesheet' type='text/css'>
<link
  href='https://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic-ext'
  rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css"
  href="resources/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="resources/css/style.css">
<c:url var="rootUrl" value="/" />
</head>
<body>
  <div class="page-header">
    <h1 class="text-center">
      <a href="${rootUrl}">News portal</a>
    </h1>
  </div>
  <div class="container-fluid">
    <div class="news">
      <div class="row">
        <div class="col-md-8">
          <strong><span class="title"><c:out
                value="${news.title}" /></span></strong>
        </div>
        <div class="col-md-2">
          (by
          <c:out value="${news.author.name}" />
          )
        </div>
        <div class="col-md-2">
          <c:out value="${news.creationDate}" />
        </div>
      </div>
      <div class="shortText">
        <div class="row">
          <div class="col-md-12">
            <c:out value="${news.fullText}" />
          </div>
        </div>
      </div>
      <div class="info">
        <div class="row">
          <div class="col-md-4 col-md-offset-6 text-right">
            <div class="tags">
              <c:forEach var="tag" items="${news.tags}">
                <span class="tag"><c:out value="${tag.name}" /></span>
              </c:forEach>
            </div>
          </div>
          <div class="col-md-2">
            Comments(
            <c:out value="${news.comments.size()}" />
            )
          </div>
        </div>
      </div>
      <div class="comments">
        <c:forEach var="comment" items="${news.comments}">
          <div class="row">
            <div class="col-md-6">
              <div class="comment">
                <c:out value="${comment.text}" />
              </div>
            </div>
          </div>
        </c:forEach>
        <form>
          <textarea class="form-control"
            placeholder="Enter comment here..." rows="4" cols="8"
            name="commentText" id="comment" ></textarea>
          <input type="hidden" name="newsId" value="${news.id}" /> <input
            type="hidden" name="command" value="add_comment" />
          <button style="margin-top: 20px;" type="submit"
            class="btn btn-default">Leave a comment</button>
        </form>
      </div>
    </div>
  </div>
  <div class="panel panel-default text-center">
    <div class="panel-footer">Copyright @ Epam 2016. All rights
      reserved.</div>
  </div>
</body>
</html>