package com.epam.lab.newsManagement.controller.command;

import java.util.HashMap;
import java.util.Map;

public class CommandHelper {

	private static final CommandHelper instance = new CommandHelper();
	
	private Map<CommandName, Command> commands;
	
	public static synchronized CommandHelper getInstance() {
		return instance;
	}
	
	private CommandHelper() {
		commands = new HashMap<>();
		commands.put(CommandName.SEARCH_NEWS, new SearchNewsCommand());
		commands.put(CommandName.GET_NEWS_BY_ID, new GetNewsByIdCommand());
		commands.put(CommandName.ADD_COMMENT, new AddCommentCommand());
	}
	
	public Command getCommand(CommandName commandName) {
		return commands.get(commandName);
	}
	
}
