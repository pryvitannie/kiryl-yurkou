package com.epam.lab.newsManagement.controller.command;

public enum CommandName {

	SEARCH_NEWS, GET_NEWS_BY_ID, ADD_COMMENT, GET_NEWS_PAGE
	
}
