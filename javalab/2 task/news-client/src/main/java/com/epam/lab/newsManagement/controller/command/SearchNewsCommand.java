package com.epam.lab.newsManagement.controller.command;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationContext;

import com.epam.lab.newsManagement.entity.Author;
import com.epam.lab.newsManagement.entity.News;
import com.epam.lab.newsManagement.entity.Tag;
import com.epam.lab.newsManagement.service.AuthorService;
import com.epam.lab.newsManagement.service.NewsService;
import com.epam.lab.newsManagement.service.ServiceException;
import com.epam.lab.newsManagement.service.TagService;
import com.epam.lab.newsManagement.service.util.SearchCriteria;
import com.epam.lab.newsManagement.viewHelper.JspName;
import com.epam.lab.newsManagement.webutil.PaginationHelper;
import com.epam.lab.newsManagement.webutil.VisiblePageBorders;

public class SearchNewsCommand implements Command {
	
	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ApplicationContext applicationContext = (ApplicationContext) req.getServletContext().getAttribute("applicationContext");
		HttpSession session = req.getSession();
		NewsService newsService = (NewsService) applicationContext.getBean("newsService");
		AuthorService authorService = (AuthorService) applicationContext.getBean("authorService");
		TagService tagService = (TagService) applicationContext.getBean("tagService");	
		PaginationHelper paginationHelper = (PaginationHelper) applicationContext.getBean("paginationHelper");
		VisiblePageBorders visiblePageBorders;		
		Integer currentPageNumber = null;
		List<News> newsList = null;
		List<Author> authors = null;	
		List<Tag> tags = null;
		Long authorId;
		List<Long> tagIdList;
		SearchCriteria searchCriteria;
		String currentPageNumberString = req.getParameter("currentPageNumber");
		String authorIdString = req.getParameter("authorId");
		String[] tagIdListString = req.getParameterValues("tagId");		
		if (currentPageNumberString != null) {
			currentPageNumber = Integer.parseInt(currentPageNumberString);		
		}
		if (authorIdString == null) {
			authorId = null;
		} else {
			authorId = Long.parseLong(authorIdString);
		}		
		if (tagIdListString == null) {
			tagIdList = null;
		} else {
			tagIdList = new ArrayList<>();
			for (String s : tagIdListString) {
				tagIdList.add(Long.parseLong(s));
			}
		}
		if (authorId != null || tagIdList != null || currentPageNumber == null) {
			searchCriteria = new SearchCriteria();
			searchCriteria.setAuthorId(authorId);
			searchCriteria.setTagIdList(tagIdList);
			session.setAttribute("searchCriteria", searchCriteria);
		} else {
			searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
		}	
		if (currentPageNumber == null) {
			currentPageNumber = paginationHelper.getFirstPage();
		}		
		try {
			paginationHelper.initizialize(newsService.getNewsCount(searchCriteria));
			currentPageNumber = paginationHelper.correctPageNumber(currentPageNumber);
			newsList = newsService.search(searchCriteria, 
					paginationHelper.getBeginRow(currentPageNumber), paginationHelper.getEndRow(currentPageNumber));
			authors = authorService.getAllAuthors();	
			tags = tagService.getAllTags();			
		} catch (ServiceException e) {
			req.setAttribute("message", e.getCause().getMessage());
			req.getRequestDispatcher(JspName.ERROR).forward(req, resp);
		}				
		visiblePageBorders = paginationHelper.calculateVisiblePageNumberBorders(currentPageNumber);
		req.setAttribute("beginPageNumber", visiblePageBorders.getBeginPageNumber());
		req.setAttribute("endPageNumber", visiblePageBorders.getEndPageNumber());
		req.setAttribute("currentPageNumber", currentPageNumber);
		req.setAttribute("newsList", newsList);
		req.setAttribute("authors", authors);	
		req.setAttribute("tags", tags);
		RequestDispatcher requestDispatcher = req.getRequestDispatcher(JspName.NEWS_LIST);
		requestDispatcher.forward(req, resp);		
	}

}
