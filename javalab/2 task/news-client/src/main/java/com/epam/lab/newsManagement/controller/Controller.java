package com.epam.lab.newsManagement.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.lab.newsManagement.controller.command.Command;
import com.epam.lab.newsManagement.controller.command.CommandHelper;
import com.epam.lab.newsManagement.controller.command.CommandName;

public class Controller extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String commandParam = req.getParameter("command");
		CommandName commandName;
		if (commandParam != null) {
			commandName = CommandName.valueOf(commandParam.toUpperCase());
		} else {
			commandName = CommandName.SEARCH_NEWS;
		}
		Command command = CommandHelper.getInstance().getCommand(commandName);
		command.execute(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
	
}
