package com.epam.lab.newsManagement.viewHelper;

public class JspName {
	
    private JspName() {}

    public static final String NEWS_LIST = "WEB-INF/views/jsp/newsList.jsp";
    public static final String SINGLE_NEWS = "WEB-INF/views/jsp/news.jsp";
    public static final String ERROR = "WEB-INF/views/jsp/error.jsp";

}
