package com.epam.lab.newsManagement.controller.command;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;

import com.epam.lab.newsManagement.entity.News;
import com.epam.lab.newsManagement.service.NewsService;
import com.epam.lab.newsManagement.service.ServiceException;
import com.epam.lab.newsManagement.viewHelper.JspName;

public class GetNewsByIdCommand implements Command {

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {				
		ApplicationContext applicationContext = (ApplicationContext) req.getServletContext().getAttribute("applicationContext");
		NewsService newsService = (NewsService) applicationContext.getBean("newsService");
		Long newsId = Long.parseLong(req.getParameter("newsId"));	
		News news = null;
		try {
			news = newsService.getNewsById(newsId);
		} catch (ServiceException e) {
			req.setAttribute("message", e.getCause().getMessage());
			req.getRequestDispatcher(JspName.ERROR).forward(req, resp);
		}
		req.setAttribute("news", news);
		RequestDispatcher requestDispatcher = req.getRequestDispatcher(JspName.SINGLE_NEWS);
		requestDispatcher.forward(req, resp);
	}

}
