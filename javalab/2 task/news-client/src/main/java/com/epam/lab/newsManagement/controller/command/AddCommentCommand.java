package com.epam.lab.newsManagement.controller.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;

import com.epam.lab.newsManagement.service.CommentService;
import com.epam.lab.newsManagement.service.ServiceException;
import com.epam.lab.newsManagement.viewHelper.JspName;

public class AddCommentCommand implements Command {

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		ApplicationContext applicationContext = (ApplicationContext) req.getServletContext().getAttribute("applicationContext");
		CommentService commentService = (CommentService) applicationContext.getBean("commentService");
		String commentText = req.getParameter("commentText");
		String newsIdString = req.getParameter("newsId");
		Long newsId = null;
		if (newsIdString != null) {
			newsId = Long.parseLong(newsIdString);
		}		
		try {
			commentService.addComment(commentText, newsId);
		} catch (ServiceException e) {
			req.setAttribute("message", e.getCause().getMessage());
			req.getRequestDispatcher(JspName.ERROR).forward(req, resp);
		}
		resp.sendRedirect("?command=get_news_by_id&newsId=" + newsIdString);
	}

}
