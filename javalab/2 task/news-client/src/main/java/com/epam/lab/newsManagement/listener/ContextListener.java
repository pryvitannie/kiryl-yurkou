package com.epam.lab.newsManagement.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ContextListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		ApplicationContext rootContext = new ClassPathXmlApplicationContext("beans.xml");
		arg0.getServletContext().setAttribute("applicationContext", rootContext);	
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		
	}

}
