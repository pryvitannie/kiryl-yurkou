package com.epam.lab.newsManagement.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;

import com.epam.lab.newsManagement.dao.impl.JdbcNewsDao;
import com.epam.lab.newsManagement.entity.Author;
import com.epam.lab.newsManagement.entity.News;

@DataSet(loadStrategy = CleanInsertLoadStrategy.class)
public class NewsDaoTest extends UnitilsJUnit4 {

	@TestDataSource
	private DataSource dataSource;
	private NewsDao newsDao;

	@Before
	public void initializeDao() {
		newsDao = new JdbcNewsDao(dataSource);
	}
	
	@Test
	@ExpectedDataSet
	public void testAddNews() {
		News news = new News();
		news.setTitle("title");
		news.setShortText("short text");
		news.setFullText("full text");
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		news.setAuthor(new Author());
		try {
			newsDao.addNews(news);
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}

	@Test
	@ExpectedDataSet
	public void testEditNews() {
		News news = new News();
		news.setId(1L);
		news.setTitle("modified title");
		news.setShortText("modified short text");
		news.setFullText("modified full text");
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		news.setAuthor(new Author());
		try {
			newsDao.editNews(news);
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}

	@Test
	@ExpectedDataSet
	public void testDeleteNews() {
		try {
			newsDao.deleteNews(2L);
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGetAllNews() {
		List<News> expectedNewsList = new ArrayList<>();
		List<News> actualNewsList = null;
		News news = new News();
		news.setId(1L);
		news.setTitle("2 automatically loaded news");
		expectedNewsList.add(news);
		news = new News();
		news.setId(1L);
		news.setTitle("1 automatically loaded news");
		expectedNewsList.add(news);
		try {
			actualNewsList = newsDao.getAllNews(1, 2);
		} catch (DaoException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < expectedNewsList.size(); i++) {
			Assert.assertEquals(expectedNewsList.get(i).getTitle(), actualNewsList.get(i).getTitle());
		}
	}

	@Test
	public void testGetNewsById() {
		News expectedNews = null;
		try {
			expectedNews = newsDao.getNewsById(2L);
		} catch (DaoException e) {
			e.printStackTrace();
		}
		Assert.assertEquals(expectedNews.getTitle(), "2 automatically loaded news");		
	}

	@Test
	public void testGetNewsByAuthorAndTags() {
		
	}

	@Test
	public void testGetNewsByAuthor() {
		
	}

	@Test
	public void testGetNewsByTags() {
		
	}

	/*@Test
	public void testGetNewsCount() {
		int newsCount = 0;
		try {
			newsCount = newsDao.getNewsCount();
		} catch (DaoException e) {
			e.printStackTrace();
		}
		Assert.assertEquals(newsCount, 2);
	}*/
	
}