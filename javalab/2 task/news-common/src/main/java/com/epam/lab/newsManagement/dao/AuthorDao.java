package com.epam.lab.newsManagement.dao;

import java.util.List;

import com.epam.lab.newsManagement.entity.Author;

public interface AuthorDao {

	Long addAuthor(Author author) throws DaoException;
	
	void editAuthor(Author author) throws DaoException;

	void addAuthorToNews(Long newsId, Long authorId) throws DaoException;

	List<Author> getAllAuthors() throws DaoException;

	Author getAuthorByNews(Long newsId) throws DaoException;

	void setAuthorExpired(Author author) throws DaoException;

	void deleteAuthor(Long authorId) throws DaoException;

}
