package com.epam.lab.newsManagement.service;

import java.util.Date;
import java.util.List;

import com.epam.lab.newsManagement.entity.News;
import com.epam.lab.newsManagement.service.util.SearchCriteria;

public interface NewsService {

	boolean addNews(String title, String shortText, String fullText,
			Long authorId, List<Long> tagIdList, Date creationDate) throws ServiceException;

	boolean editNews(Long newsId, String title, String shortText, String fullText,
			Long authorId, List<Long> tagIdList, Date modificationDate) throws ServiceException;

	boolean deleteNews(Long newsId) throws ServiceException;

	News getNewsById(Long newsId) throws ServiceException;

	List<News> search(SearchCriteria searchCriteria, int beginRow, int endRow) throws ServiceException;

	int getNewsCount(SearchCriteria searchCriteria) throws ServiceException;

}
