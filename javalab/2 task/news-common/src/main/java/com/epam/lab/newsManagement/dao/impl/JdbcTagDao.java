package com.epam.lab.newsManagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.lab.newsManagement.dao.DaoException;
import com.epam.lab.newsManagement.dao.TagDao;
import com.epam.lab.newsManagement.entity.Tag;

public class JdbcTagDao extends JdbcDao implements TagDao {

	private static final String SELECT_ALL_TAGS = "SELECT TAG_ID, TAG_NAME"
			+ " FROM TAGS";
	private static final String INSERT_TAG_SQL = "INSERT INTO TAGS" 
			+ " (TAG_ID, TAG_NAME)" 
			+ " VALUES (?, ?)";
	private static final String UPDATE_TAG_SQL = "UPDATE TAGS"
			+ " SET TAG_NAME = ?"
			+ " WHERE TAG_ID = ?";
	private static final String INSERT_TAG_TO_NEWS_SQL = "INSERT INTO NEWS_TAGS"
			+ " (NEWS_ID, TAG_ID)"
			+ " VALUES (?, ?)";
	private static final String REMOVE_TAGS_FROM_NEWS_SQL = "DELETE FROM NEWS_TAGS WHERE NEWS_ID = ?";
	private static final String SELECT_TAGS_BY_NEWS_SQL = "SELECT TAGS.TAG_ID, TAGS.TAG_NAME" 
			+ " FROM TAGS"
			+ " JOIN NEWS_TAGS ON TAGS.TAG_ID = NEWS_TAGS.TAG_ID" 
			+ " WHERE NEWS_TAGS.NEWS_ID = ?";
	private static final String GET_NEXT_TAG_ID_SQL = "SELECT TAG_ID_SEQ.NEXTVAL FROM dual";
	private static final String DELETE_TAG_SQL = "DELETE FROM TAGS" 
			+ " WHERE TAG_ID = ?";
	
	public JdbcTagDao(DataSource dataSource) {
		super(dataSource);
	}
	
	@Override
	public List<Tag> getAllTags() throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Tag> tags = new ArrayList<>();
		Tag tag;
		try {
			preparedStatement = connection.prepareStatement(SELECT_ALL_TAGS);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				tag = new Tag();
				tag.setId(resultSet.getLong("TAG_ID"));
				tag.setName(resultSet.getString("TAG_NAME"));
				tags.add(tag);
			}
		} catch (SQLException e) {
			throw new DaoException("Can't get all tags", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
		return tags;
	}

	@Override
	public void addTagToNews(Long newsId, Long tagId) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(INSERT_TAG_TO_NEWS_SQL);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, tagId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Can't add tag to news", e);
		} finally {
			closeResources(preparedStatement, connection);
		}
	}
	
	@Override
	public void removeTagsFromNews(Long newsId) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(REMOVE_TAGS_FROM_NEWS_SQL);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Can't remove tags from news", e);
		} finally {
			closeResources(preparedStatement, connection);
		}
	}

	@Override
	public Long addTag(Tag tag) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		try {
			tag.setId(getNextTagId());
			preparedStatement = connection.prepareStatement(INSERT_TAG_SQL);
			preparedStatement.setLong(1, tag.getId());
			preparedStatement.setString(2, tag.getName());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Can't add tag", e);
		} finally {
			closeResources(preparedStatement, connection);
		}
		return tag.getId();
	}

	@Override
	public void editTag(Tag tag) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(UPDATE_TAG_SQL);			
			preparedStatement.setString(1, tag.getName());
			preparedStatement.setLong(2, tag.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Can't edit tag", e);
		} finally {
			closeResources(preparedStatement, connection);
		}
	}
	
	@Override
	public List<Tag> getTagsByNews(Long newsId) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Tag> tags = new ArrayList<>();
		Tag tag;
		try {
			preparedStatement = connection.prepareStatement(SELECT_TAGS_BY_NEWS_SQL);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				tag = new Tag();
				tag.setId(resultSet.getLong("TAG_ID"));
				tag.setName(resultSet.getString("TAG_NAME"));
				tags.add(tag);
			}
		} catch (SQLException e) {
			throw new DaoException("Can't get tags by news", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
		return tags;
	}

	private Long getNextTagId() throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = connection.prepareStatement(GET_NEXT_TAG_ID_SQL);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			return resultSet.getLong(1);
		} catch (SQLException e) {
			throw new DaoException("Can't get next tag id", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
	}

	@Override
	public void deleteTag(Long tagId) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		int result;
		try {
			preparedStatement = connection.prepareStatement(DELETE_TAG_SQL);
			preparedStatement.setLong(1, tagId);
			result = preparedStatement.executeUpdate();
			if (result == 0) {
				throw new DaoException("There is no tag having id = " + tagId);
			}
		} catch (SQLException e) {
			throw new DaoException("Can't delete tag", e);
		} finally {
			closeResources(preparedStatement, connection);
		}
	}

}
