package com.epam.lab.newsManagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.lab.newsManagement.dao.DaoException;

public class JdbcDao {

	private DataSource dataSource;

	public JdbcDao(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Connection getConnection() throws DaoException {
		Connection connection = null;
		connection = DataSourceUtils.getConnection(dataSource);
		return connection;
	}

	public void closeResources(ResultSet resultSet, PreparedStatement preparedStatement, Connection connection)
			throws DaoException {
		try {
			if (resultSet != null) {
				resultSet.close();
			}
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null) {
				DataSourceUtils.releaseConnection(connection, dataSource);
			}
		} catch (SQLException e) {
			throw new DaoException("Can't close resources", e);
		}
	}

	public void closeResources(PreparedStatement preparedStatement, Connection connection) throws DaoException {
		closeResources(null, preparedStatement, connection);
	}
	
}
