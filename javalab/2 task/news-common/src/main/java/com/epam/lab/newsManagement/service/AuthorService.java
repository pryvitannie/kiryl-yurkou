package com.epam.lab.newsManagement.service;

import java.util.List;

import com.epam.lab.newsManagement.entity.Author;

public interface AuthorService {

	boolean addAuthor(String name) throws ServiceException;
	
	boolean editAuthor(Long authorId, String name) throws ServiceException;

	boolean addAuthorToNews(Long newsId, Long authorId) throws ServiceException;

	List<Author> getAllAuthors() throws ServiceException;

	Author getAuthorByNews(Long newsId) throws ServiceException;

	boolean setAuthorExpired(Long authorId) throws ServiceException;

	boolean deleteAuthor(Long authorId) throws ServiceException;

}
