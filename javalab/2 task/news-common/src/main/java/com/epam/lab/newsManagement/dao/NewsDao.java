package com.epam.lab.newsManagement.dao;

import java.util.List;

import com.epam.lab.newsManagement.entity.News;

public interface NewsDao {

	Long addNews(News news) throws DaoException;

	void editNews(News news) throws DaoException;

	void deleteNews(Long newsId) throws DaoException;

	List<News> getAllNews(int beginRow, int endRow) throws DaoException;

	News getNewsById(Long newsId) throws DaoException;

	List<News> getNewsByAuthorAndTags(Long authorId, List<Long> tags, int beginRow, int endRow) throws DaoException;

	List<News> getNewsByAuthor(Long authorId, int beginRow, int endRow) throws DaoException;

	List<News> getNewsByTags(List<Long> tagIdList, int beginRow, int endRow) throws DaoException;

	int getAllNewsCount() throws DaoException;

	int getNewsByAuthorAndTagsCount(Long authorId, List<Long> tags) throws DaoException;

	int getNewsByAuthorCount(Long authorId) throws DaoException;

	int getNewsByTagsCount(List<Long> tagIdList) throws DaoException;
	

}
