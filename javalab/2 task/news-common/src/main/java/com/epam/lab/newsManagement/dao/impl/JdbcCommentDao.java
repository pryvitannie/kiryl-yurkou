package com.epam.lab.newsManagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.lab.newsManagement.dao.CommentDao;
import com.epam.lab.newsManagement.dao.DaoException;
import com.epam.lab.newsManagement.entity.Comment;

public class JdbcCommentDao extends JdbcDao implements CommentDao {

	private static final String INSERT_COMMENT_SQL = "INSERT INTO COMMENTS" 
			+ " (COMMENT_TEXT, CREATION_DATE, NEWS_ID)"
			+ " VALUES (?, ?, ?)";
	private static final String DELETE_COMMENT_SQL = "DELETE FROM COMMENTS" 
			+ " WHERE COMMENT_ID = ?";
	private static final String SELECT_COMMENTS_BY_NEWS_SQL = "SELECT COMMENT_ID, COMMENT_TEXT, CREATION_DATE"
			+ " FROM COMMENTS" 
			+ " WHERE NEWS_ID = ?" 
			+ " ORDER BY CREATION_DATE DESC";

	public JdbcCommentDao(DataSource dataSource) {
		super(dataSource);
	}
	
	@Override
	public Long addComment(Comment comment) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = connection.prepareStatement(INSERT_COMMENT_SQL);
			preparedStatement.setString(1, comment.getText());
			preparedStatement.setTimestamp(2, new Timestamp(comment.getCreationDate().getTime()));
			preparedStatement.setLong(3, comment.getNewsId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Can't add comment", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
		return comment.getId();
	}

	@Override
	public void deleteComment(Long commentId) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		int result;
		try {
			preparedStatement = connection.prepareStatement(DELETE_COMMENT_SQL);
			preparedStatement.setLong(1, commentId);
			result = preparedStatement.executeUpdate();
			if (result == 0) {
				throw new DaoException("There is no comment having id = " + commentId);
			}
		} catch (SQLException e) {
			throw new DaoException("Can't delete comment", e);
		} finally {
			closeResources(preparedStatement, connection);
		}
	}

	@Override
	public List<Comment> getCommentsByNews(Long newsId) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Comment> comments = new ArrayList<>();
		Comment comment;
		try {
			preparedStatement = connection.prepareStatement(SELECT_COMMENTS_BY_NEWS_SQL);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				comment = new Comment();
				comment.setId(resultSet.getLong("COMMENT_ID"));
				comment.setText(resultSet.getString("COMMENT_TEXT"));
				comment.setCreationDate(resultSet.getTimestamp("CREATION_DATE"));
				comments.add(comment);
			}
		} catch (SQLException e) {
			throw new DaoException("Can't add comments to news", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
		return comments;
	}

}
