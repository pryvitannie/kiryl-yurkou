package com.epam.lab.newsManagement.service;

import java.util.List;

import com.epam.lab.newsManagement.entity.Tag;

public interface TagService {

	boolean addTag(String name) throws ServiceException;
	
	boolean editTag(Long tagId, String name) throws ServiceException;

	List<Tag> getAllTags() throws ServiceException;
	
	List<Tag> getTagsByNews(Long newsId) throws ServiceException;

	boolean addTagToNews(Long newsId, Long tagId) throws ServiceException;
	
	boolean removeTagFormNews(Long newsId) throws ServiceException;

	boolean deleteTag(Long tagId) throws ServiceException;

}
