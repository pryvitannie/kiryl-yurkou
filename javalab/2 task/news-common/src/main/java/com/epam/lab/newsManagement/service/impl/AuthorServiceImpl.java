package com.epam.lab.newsManagement.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.lab.newsManagement.dao.AuthorDao;
import com.epam.lab.newsManagement.dao.DaoException;
import com.epam.lab.newsManagement.entity.Author;
import com.epam.lab.newsManagement.service.AuthorService;
import com.epam.lab.newsManagement.service.ServiceException;

public class AuthorServiceImpl implements AuthorService {

	private final static Logger LOGGER = LogManager.getRootLogger();

	private AuthorDao authorDao;

	@Override
	public boolean addAuthor(String name) throws ServiceException {
		Author author = new Author();
		Long authorId;
		author.setName(name);
		try {
			authorId = authorDao.addAuthor(author);
		} catch (DaoException e) {
			LOGGER.debug("ServiceException was thrown in AuthorService::addAuthor, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("Author having id = " + authorId + " has been added");
		return true;
	}

	@Override
	public boolean editAuthor(Long authorId, String name) throws ServiceException {
		Author author = new Author();
		author.setId(authorId);
		author.setName(name);
		try {
			authorDao.editAuthor(author);
		} catch (DaoException e) {
			LOGGER.debug("ServiceException was thrown in AuthorService::editAuthor, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("Author has been edited");
		return true;
	}
	
	@Override
	public boolean addAuthorToNews(Long newsId, Long authorId) throws ServiceException {
		try {
			authorDao.addAuthorToNews(newsId, authorId);
		} catch (DaoException e) {
			LOGGER.debug("ServiceException was thrown in AuthorService::addAuthorToNews, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("Author having id = " + authorId + " has beeb added to news having id = " + newsId);
		return true;
	}

	@Override
	public List<Author> getAllAuthors() throws ServiceException {
		List<Author> authors = null;
		try {
			authors = authorDao.getAllAuthors();
		} catch (DaoException e) {
			LOGGER.debug("ServiceException was thrown in AuthorService::getAllAuthors, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("Authors has been recieved");
		return authors;
	}

	@Override
	public Author getAuthorByNews(Long newsId) throws ServiceException {
		Author author = null;
		try {
			author = authorDao.getAuthorByNews(newsId);
		} catch (DaoException e) {
			LOGGER.debug("ServiceException was thrown in AuthorService::getAuthorByNews, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("Author of news having id = " + newsId + " has been recieved");
		return author;
	}

	@Override
	public boolean setAuthorExpired(Long authorId) throws ServiceException {
		Calendar calendar = new GregorianCalendar();
		Date currentDate = calendar.getTime();
		Author author = new Author();
		author.setId(authorId);
		author.setExpired(currentDate);
		try {
			authorDao.setAuthorExpired(author);
		} catch (DaoException e) {
			LOGGER.debug("ServiceException was thrown in AuthorService::expireAuthor, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("Author having id = " + authorId + " has been set expired");
		return true;
	}

	@Override
	public boolean deleteAuthor(Long authorId) throws ServiceException {
		try {
			authorDao.deleteAuthor(authorId);
		} catch (DaoException e) {
			LOGGER.debug("ServiceException was thrown in AuthorService::deleteAuthor, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("Author having id = " + authorId + " has been deleted");
		return true;
	}

	public void setAuthorDao(AuthorDao authorDao) {
		this.authorDao = authorDao;
	}

}
