package com.epam.lab.newsManagement.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.lab.newsManagement.dao.CommentDao;
import com.epam.lab.newsManagement.dao.DaoException;
import com.epam.lab.newsManagement.entity.Comment;
import com.epam.lab.newsManagement.service.CommentService;
import com.epam.lab.newsManagement.service.ServiceException;

public class CommentServiceImpl implements CommentService {

	private final static Logger LOGGER = LogManager.getRootLogger();

	private CommentDao commentDao;

	@Override
	public boolean addComment(String text, Long newsId) throws ServiceException {
		Calendar calendar = new GregorianCalendar();
		Date currentDate = calendar.getTime();
		Comment comment = new Comment();
		comment.setCreationDate(currentDate);
		comment.setText(text);
		comment.setNewsId(newsId);
		try {
			commentDao.addComment(comment);
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in CommentService::addComment, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("Comment to news having id = " + newsId + " has been added");
		return true;
	}

	@Override
	public boolean deleteComment(Long commentId) throws ServiceException {
		try {
			commentDao.deleteComment(commentId);
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in CommentService::deleteCommentNews, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("Comment having id = " + commentId + " has been deleted");
		return true;
	}

	@Override
	public List<Comment> getCommentsByNews(Long newsId) throws ServiceException {
		List<Comment> comments = new ArrayList<>();
		try {
			comments = commentDao.getCommentsByNews(newsId);
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in CommentService::getCommentsByNews, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("Comments to news having id = " + newsId + " has been recieved");
		return comments;
	}

	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}

}
