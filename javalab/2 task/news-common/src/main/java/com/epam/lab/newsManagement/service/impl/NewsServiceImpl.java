package com.epam.lab.newsManagement.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.lab.newsManagement.dao.DaoException;
import com.epam.lab.newsManagement.dao.NewsDao;
import com.epam.lab.newsManagement.entity.News;
import com.epam.lab.newsManagement.service.AuthorService;
import com.epam.lab.newsManagement.service.CommentService;
import com.epam.lab.newsManagement.service.NewsService;
import com.epam.lab.newsManagement.service.ServiceException;
import com.epam.lab.newsManagement.service.TagService;
import com.epam.lab.newsManagement.service.util.SearchCriteria;

public class NewsServiceImpl implements NewsService {

	private final static Logger LOGGER = LogManager.getRootLogger();

	private NewsDao newsDao;
	private AuthorService authorService;
	private TagService tagService;
	private CommentService commentService;

	@Override
	@Transactional(rollbackFor = ServiceException.class)
	public boolean addNews(String title, String shortText, String fullText,
			Long authorId, List<Long> tagIdList, Date creationDate) throws ServiceException 
	{
		News news = new News();
		Long newsId;
		news.setTitle(title);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setCreationDate(creationDate);
		news.setModificationDate(creationDate);
		try {
			newsId = newsDao.addNews(news);
			authorService.addAuthorToNews(newsId, authorId);
			if (tagIdList != null) {
				for (Long tagId : tagIdList) {
					tagService.addTagToNews(newsId, tagId);
				}				
			}
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in NewsService::addNews, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("News having id = " + newsId +  " has been added");
		return true;
	}

	@Override
	public boolean editNews(Long newsId, String title, String shortText, String fullText,
			Long authorId, List<Long> tagIdList, Date modificationDate) throws ServiceException 
	{
		News news = new News();
		news.setId(newsId);
		news.setTitle(title);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setModificationDate(modificationDate);
		try {
			newsDao.editNews(news);		
			tagService.removeTagFormNews(newsId);
			if (tagIdList != null) {
				for (Long tagId : tagIdList) {
					tagService.addTagToNews(newsId, tagId);
				}				
			}
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in NewsService::editNews, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("News having id = " + newsId + " has been edited");
		return true;
	}

	@Override
	public boolean deleteNews(Long newsId) throws ServiceException {
		try {
			newsDao.deleteNews(newsId);
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in NewsService::deleteNews, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("News having id = " + newsId + " has been deleted");
		return true;
	}

	@Override
	public News getNewsById(Long newsId) throws ServiceException {
		News news;
		try {
			news = newsDao.getNewsById(newsId);
			setNewsInfo(news);
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in NewsService::getNewsById, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("News having id = " + newsId + " has been received");
		return news;
	}

	@Override
	public List<News> search(SearchCriteria searchCriteria, int beginRow, int endRow) throws ServiceException {
		List<News> newsList = new ArrayList<>();
		try {
			if (searchCriteria.getAuthorId() != null) {
				if (searchCriteria.getTagIdList() != null) {
					newsList = newsDao.getNewsByAuthorAndTags(searchCriteria.getAuthorId(),
							searchCriteria.getTagIdList(), beginRow, endRow);
				} else {
					newsList = newsDao.getNewsByAuthor(searchCriteria.getAuthorId(), beginRow, endRow);
				}
			} else if (searchCriteria.getTagIdList() != null) {
				newsList = newsDao.getNewsByTags(searchCriteria.getTagIdList(), beginRow, endRow);
			} else {
				newsList = newsDao.getAllNews(beginRow, endRow);
			}
			for (News news : newsList) {
				setNewsInfo(news);
			}
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in NewsService::search, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("News has been found by search criteria");
		return newsList;
	}

	@Override
	public int getNewsCount(SearchCriteria searchCriteria) throws ServiceException {
		int count = 0;
		try {
			if (searchCriteria.getAuthorId() != null) {
				if (searchCriteria.getTagIdList() != null) {
					count = newsDao.getNewsByAuthorAndTagsCount(searchCriteria.getAuthorId(),
							searchCriteria.getTagIdList());
				} else {
					count = newsDao.getNewsByAuthorCount(searchCriteria.getAuthorId());
				}
			} else if (searchCriteria.getTagIdList() != null) {
				count = newsDao.getNewsByTagsCount(searchCriteria.getTagIdList());
			} else {
				count = newsDao.getAllNewsCount();
			}
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in NewsService::getNewsCount, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("There are " + count + " news");
		return count;
	}

	private void setNewsInfo(News news) {
		try {
			news.setAuthor(authorService.getAuthorByNews(news.getId()));
			news.setTags(tagService.getTagsByNews(news.getId()));
			news.setComments(commentService.getCommentsByNews(news.getId()));
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}
	
	public CommentService getCommentService() {
		return commentService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public void setNewsDao(NewsDao newsDao) {
		this.newsDao = newsDao;
	}
	
}
