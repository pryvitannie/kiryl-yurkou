package com.epam.lab.newsManagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.lab.newsManagement.dao.AuthorDao;
import com.epam.lab.newsManagement.dao.DaoException;
import com.epam.lab.newsManagement.entity.Author;

public class JdbcAuthorDao extends JdbcDao implements AuthorDao {

	private static final String INSERT_AUTHOR_SQL = "INSERT INTO AUTHORS" 
			+ " (AUTHOR_ID, AUTHOR_NAME, EXPIRED)"
			+ " VALUES (?, ?, ?)";
	private static final String UPDATE_AUTHOR_SQL = "UPDATE AUTHORS"
			+ " SET AUTHOR_NAME = ?"
			+ " WHERE AUTHOR_ID = ?";
	private static final String INSERT_AUTHOR_TO_NEWS_SQL = "INSERT INTO NEWS_AUTHORS"
			+ " (NEWS_ID, AUTHOR_ID)"
			+ " VALUES (?, ?)";
	private static final String SELECT_ALL_AUTHORS_SQL = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED"
			+ " FROM AUTHORS"
			+ " WHERE EXPIRED IS NULL" 
			+ " ORDER BY AUTHOR_ID";
	private static final String SELECT_AUTHOR_BY_NEWS_SQL = "SELECT AUTHORS.AUTHOR_ID, AUTHOR_NAME, EXPIRED"
			+ " FROM AUTHORS"
			+ " JOIN NEWS_AUTHORS ON AUTHORS.AUTHOR_ID = NEWS_AUTHORS.AUTHOR_ID"
			+ " WHERE NEWS_AUTHORS.NEWS_ID = ?";
	private static final String EXPIRE_AUTHOR_SQL = "UPDATE AUTHORS" 
			+ " SET EXPIRED = ?"
			+ " WHERE AUTHOR_ID = ?";
	private static final String DELETE_AUTHOR_SQL = "DELETE FROM AUTHORS" 
			+ " WHERE AUTHOR_ID = ?";
	private static final String GET_NEXT_AUTHOR_ID_SQL = "SELECT AUTHOR_ID_SEQ.NEXTVAL FROM dual";

	public JdbcAuthorDao(DataSource dataSource) {
		super(dataSource);
	}
	
	@Override
	public Long addAuthor(Author author) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		author.setId(getNextAuthorId());
		try {
			preparedStatement = connection.prepareStatement(INSERT_AUTHOR_SQL);
			preparedStatement.setLong(1, author.getId());
			preparedStatement.setString(2, author.getName());
			if (author.getExpired() != null) {
				preparedStatement.setTimestamp(2, new Timestamp(author.getExpired().getTime()));
			} else {
				preparedStatement.setTimestamp(2, null);
			}
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Can't add author", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
		return author.getId();
	}
	
	@Override
	public void editAuthor(Author author) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = connection.prepareStatement(UPDATE_AUTHOR_SQL);			
			preparedStatement.setString(1, author.getName());
			preparedStatement.setLong(2, author.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Can't edit author", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
	}

	@Override
	public void addAuthorToNews(Long newsId, Long authorId) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		int result;
		try {
			preparedStatement = connection.prepareStatement(INSERT_AUTHOR_TO_NEWS_SQL);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			result = preparedStatement.executeUpdate();
			if (result == 0) {
				throw new DaoException("Can't add author to news.");
			}
		} catch (SQLException e) {
			throw new DaoException("Can't add author to news. There is no author having id = " + authorId, e);
		} finally {
			closeResources(preparedStatement, connection);
		}
	}

	@Override
	public List<Author> getAllAuthors() throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Author> authors = new ArrayList<>();
		Author author;
		try {
			preparedStatement = connection.prepareStatement(SELECT_ALL_AUTHORS_SQL);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				author = new Author();
				author.setId(resultSet.getLong("AUTHOR_ID"));
				author.setName(resultSet.getString("AUTHOR_NAME"));
				author.setExpired(resultSet.getTimestamp("EXPIRED"));
				authors.add(author);
			}
		} catch (SQLException e) {
			throw new DaoException("Can't get authors", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
		return authors;
	}

	@Override
	public Author getAuthorByNews(Long newsId) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Author author = null;
		try {
			preparedStatement = connection.prepareStatement(SELECT_AUTHOR_BY_NEWS_SQL);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				author = new Author();
				author.setId(resultSet.getLong("AUTHOR_ID"));
				author.setName(resultSet.getString("AUTHOR_NAME"));
				author.setExpired(resultSet.getTimestamp("EXPIRED"));
			}
		} catch (SQLException e) {
			throw new DaoException("Can't get author by news", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
		return author;
	}

	@Override
	public void setAuthorExpired(Author author) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		int result;
		try {
			preparedStatement = connection.prepareStatement(EXPIRE_AUTHOR_SQL);
			preparedStatement.setTimestamp(1, new Timestamp(author.getExpired().getTime()));
			preparedStatement.setLong(2, author.getId());
			result = preparedStatement.executeUpdate();
			if (result == 0) {
				throw new DaoException("There is no author having id = " + author.getId());
			}
		} catch (SQLException e) {
			throw new DaoException("Can't expire author", e);
		} finally {
			closeResources(preparedStatement, connection);
		}
	}

	@Override
	public void deleteAuthor(Long authorId) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		int result;
		try {
			preparedStatement = connection.prepareStatement(DELETE_AUTHOR_SQL);
			preparedStatement.setLong(1, authorId);
			result = preparedStatement.executeUpdate();
			if (result == 0) {
				throw new DaoException("There is no news having id = " + authorId);
			}
		} catch (SQLException e) {
			throw new DaoException("Can't delete news", e);
		} finally {
			closeResources(preparedStatement, connection);
		}
	}

	private Long getNextAuthorId() throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Long authorId;
		try {
			preparedStatement = connection.prepareStatement(GET_NEXT_AUTHOR_ID_SQL);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			authorId = resultSet.getLong(1);
		} catch (SQLException e) {
			throw new DaoException("Can't get next author id", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
		return authorId;
	}

}
