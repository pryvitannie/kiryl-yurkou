package com.epam.lab.newsManagement.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.lab.newsManagement.dao.DaoException;
import com.epam.lab.newsManagement.dao.TagDao;
import com.epam.lab.newsManagement.entity.Tag;
import com.epam.lab.newsManagement.service.ServiceException;
import com.epam.lab.newsManagement.service.TagService;

public class TagServiceImpl implements TagService {

	private final static Logger LOGGER = LogManager.getRootLogger();

	private TagDao tagDao;

	@Override
	public boolean addTag(String name) throws ServiceException {
		Tag tag = new Tag();
		tag.setName(name);
		try {
			tagDao.addTag(tag);
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in TagService::addTag, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("Tag has been added");
		return true;
	}
	
	@Override
	public boolean editTag(Long tagId, String name) throws ServiceException {
		Tag tag = new Tag();
		tag.setId(tagId);
		tag.setName(name);
		try {
			tagDao.editTag(tag);
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in TagService::editTag, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("Tag having id = " + tagId + " has been edited");
		return true;
	}
	
	@Override
	public List<Tag> getAllTags() throws ServiceException {
		List<Tag> tags = null;
		try {
			tags = tagDao.getAllTags();
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in TagService::getAllTags, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("Tags has been recieved");
		return tags;
	}

	@Override
	public List<Tag> getTagsByNews(Long newsId) throws ServiceException {
		List<Tag> tags = null;
		try {
			tags = tagDao.getTagsByNews(newsId);
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in TagService::getTagsByNews, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("Tags of news having id = " + newsId + " has been recieved");
		return tags;
	}	

	@Override
	public boolean addTagToNews(Long newsId, Long tagId) throws ServiceException {
		try {
			tagDao.addTagToNews(newsId, tagId);			
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in TagService::addTagToNews, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("Tag has been added to news having id = " + newsId);
		return true;
	}
	
	@Override
	public boolean removeTagFormNews(Long newsId) throws ServiceException {
		try {
			tagDao.removeTagsFromNews(newsId);			
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in TagService::removeTagFromNews, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("Tags has been deleted from news having id = " + newsId);
		return true;
	}

	@Override
	public boolean deleteTag(Long tagId) throws ServiceException {
		try {
			tagDao.deleteTag(tagId);			
		} catch (DaoException e) {
			LOGGER.debug("DaoException was thrown in TagService::deleteTag, full stack trace follows", e);
			throw new ServiceException(e);
		}
		LOGGER.debug("Tag having id = " + tagId + " has been deleted");
		return true;
	}
	
	public void setTagDao(TagDao tagDao) {
		this.tagDao = tagDao;
	}

}
