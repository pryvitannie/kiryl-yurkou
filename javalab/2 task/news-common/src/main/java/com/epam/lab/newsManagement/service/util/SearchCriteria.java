package com.epam.lab.newsManagement.service.util;

import java.util.ArrayList;
import java.util.List;

public class SearchCriteria {

	private Long authorId = null;
	private List<Long> tagIdList = null;

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public List<Long> getTagIdList() {
		return tagIdList;
	}
	
	public void setTagIdList(List<Long> tagIdList) {
		this.tagIdList = tagIdList;
	}

	public void addTagId(Long tagId) {
		if (tagIdList == null) {
			tagIdList = new ArrayList<>();
		}
		tagIdList.add(tagId);
	}

}
