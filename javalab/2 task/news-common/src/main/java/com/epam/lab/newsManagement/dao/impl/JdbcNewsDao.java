package com.epam.lab.newsManagement.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.lab.newsManagement.dao.DaoException;
import com.epam.lab.newsManagement.dao.NewsDao;
import com.epam.lab.newsManagement.entity.News;

public class JdbcNewsDao extends JdbcDao implements NewsDao {

	private static final String INSERT_NEWS_SQL = "INSERT INTO NEWS"
			+ " (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)"
			+ " VALUES (?, ?, ?, ?, ?, ?)";
	private static final String UPDATE_NEWS_SQL = "UPDATE NEWS"
			+ " SET TITLE = ?, SHORT_TEXT = ?, FULL_TEXT = ?, MODIFICATION_DATE = ?" + " WHERE NEWS_ID = ?";
	private static final String DELETE_NEWS_SQL = "DELETE FROM NEWS" + " WHERE NEWS_ID = ?";
	private static final String SELECT_ALL_NEWS_SQL = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM"
			+ " (SELECT NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE," 
			+ " ROW_NUMBER() OVER (ORDER BY MODIFICATION_DATE DESC, COUNT(COMMENTS.NEWS_ID) DESC) AS RN"
			+ " FROM NEWS"
			+ " LEFT JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID"
			+ " GROUP BY NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE)"
			+ " WHERE RN BETWEEN ? AND ?";
	private static final String SELECT_NEWS_BY_ID_SQL = "SELECT NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE"
			+ " FROM NEWS" 
			+ " WHERE NEWS_ID = ?";
	private static final String SELECT_NEWS_BY_AUTHOR_AND_TAGS_BEGIN = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM"
			+ " (SELECT FILTERED_NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, FILTERED_NEWS.CREATION_DATE, MODIFICATION_DATE,"
			+ " ROW_NUMBER() OVER (ORDER BY MODIFICATION_DATE DESC, COUNT(COMMENTS.NEWS_ID) DESC) AS RN"
			+ " FROM (SELECT NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE"
			+ " FROM NEWS"
			+ " JOIN NEWS_TAGS ON NEWS.NEWS_ID = NEWS_TAGS.NEWS_ID"
			+ " JOIN NEWS_AUTHORS ON NEWS.NEWS_ID = NEWS_AUTHORS.NEWS_ID"
			+ " WHERE NEWS_TAGS.TAG_ID IN";
	private static final String SELECT_NEWS_BY_AUTHOR_AND_TAGS_END = " AND NEWS_AUTHORS.AUTHOR_ID = ?"
			+ " GROUP BY NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE"
			+ " HAVING COUNT(*) = ?) FILTERED_NEWS"
			+ " LEFT JOIN COMMENTS ON COMMENTS.NEWS_ID = FILTERED_NEWS.NEWS_ID"
			+ " GROUP BY FILTERED_NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, FILTERED_NEWS.CREATION_DATE, MODIFICATION_DATE)"
			+ " WHERE RN BETWEEN ? AND ?";
	private static final String SELECT_NEWS_BY_AUTHOR_SQL = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM"
			+ " (SELECT FILTERED_NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, FILTERED_NEWS.CREATION_DATE, MODIFICATION_DATE,"
			+ " ROW_NUMBER() OVER (ORDER BY MODIFICATION_DATE DESC, COUNT(COMMENTS.NEWS_ID) DESC) AS RN"
			+ " FROM (SELECT NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE"
			+ " FROM NEWS"
			+ " JOIN NEWS_AUTHORS ON NEWS.NEWS_ID = NEWS_AUTHORS.NEWS_ID"
			+ " WHERE NEWS_AUTHORS.AUTHOR_ID = ?) FILTERED_NEWS"
			+ " LEFT JOIN COMMENTS ON COMMENTS.NEWS_ID = FILTERED_NEWS.NEWS_ID"
			+ " GROUP BY FILTERED_NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, FILTERED_NEWS.CREATION_DATE, MODIFICATION_DATE"
			+ " ORDER BY MODIFICATION_DATE DESC, COUNT(COMMENTS.NEWS_ID) DESC)"
			+ " WHERE RN BETWEEN ? AND ?";
	private static final String SELECT_NEWS_BY_TAGS_BEGIN = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM"
			+ " (SELECT FILTERED_NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, FILTERED_NEWS.CREATION_DATE, MODIFICATION_DATE,"
			+ " ROW_NUMBER() OVER (ORDER BY MODIFICATION_DATE DESC, COUNT(COMMENTS.NEWS_ID) DESC) AS RN"
			+ " FROM (SELECT NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE"
			+ " FROM NEWS"
			+ " JOIN NEWS_TAGS ON NEWS.NEWS_ID = NEWS_TAGS.NEWS_ID"
			+ " WHERE NEWS_TAGS.TAG_ID IN ";
	private static final String SELECT_NEWS_BY_TAGS_END = " GROUP BY NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE"
			+ " HAVING COUNT(*) = ?) FILTERED_NEWS"
			+ " LEFT JOIN COMMENTS ON COMMENTS.NEWS_ID = FILTERED_NEWS.NEWS_ID"
			+ " GROUP BY FILTERED_NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, FILTERED_NEWS.CREATION_DATE, MODIFICATION_DATE"
			+ " ORDER BY MODIFICATION_DATE DESC, COUNT(COMMENTS.NEWS_ID) DESC)"
			+ " WHERE RN BETWEEN ? AND ?";
	private static final String SELECT_ALL_NEWS_COUNT_SQL = "SELECT COUNT(*) FROM NEWS";
	private static final String SELECT_NEWS_BY_AUTHOR_AND_TAGS_COUNT_BEGIN = "SELECT COUNT(*)"
			+ " FROM (SELECT NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE"
			+ " FROM NEWS"
			+ " JOIN NEWS_TAGS ON NEWS.NEWS_ID = NEWS_TAGS.NEWS_ID"
			+ " JOIN NEWS_AUTHORS ON NEWS.NEWS_ID = NEWS_AUTHORS.NEWS_ID"
			+ " WHERE NEWS_TAGS.TAG_ID IN";
	private static final String SELECT_NEWS_BY_AUTHOR_AND_TAGS_COUNT_END = " AND NEWS_AUTHORS.AUTHOR_ID = ?"
			+ " GROUP BY NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE"
			+ " HAVING COUNT(*) = ?) FILTERED_NEWS";
	private static final String SELECT_NEWS_BY_TAGS_COUNT_BEGIN = "SELECT COUNT(*)"
			+ " FROM (SELECT NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE"
			+ " FROM NEWS"
			+ " JOIN NEWS_TAGS ON NEWS.NEWS_ID = NEWS_TAGS.NEWS_ID"
			+ " WHERE NEWS_TAGS.TAG_ID IN";
	private static final String SELECT_NEWS_BY_TAGS_COUNT_END = " GROUP BY NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE"
			+ " HAVING COUNT(*) = ?) FILTERED_NEWS";
	private static final String SELECT_NEWS_BY_AUTHOR_COUNT = "SELECT COUNT(*)"
			+ " FROM (SELECT NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE"
			+ " FROM NEWS"
			+ " JOIN NEWS_AUTHORS ON NEWS.NEWS_ID = NEWS_AUTHORS.NEWS_ID"
			+ " WHERE NEWS_AUTHORS.AUTHOR_ID = ?)";
	private static final String GET_NEXT_NEWS_ID_SQL = "SELECT NEWS_ID_SEQ.NEXTVAL FROM dual";

	public JdbcNewsDao(DataSource dataSource) {
		super(dataSource);
	}
	
	@Override
	public Long addNews(News news) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		int result;
		try {
			news.setId(getNextNewsId());
			preparedStatement = connection.prepareStatement(INSERT_NEWS_SQL);
			preparedStatement.setLong(1, news.getId());
			preparedStatement.setString(2, news.getTitle());
			preparedStatement.setString(3, news.getShortText());
			preparedStatement.setString(4, news.getFullText());
			preparedStatement.setTimestamp(5, new Timestamp(news.getCreationDate().getTime()));
			preparedStatement.setDate(6, new Date(news.getModificationDate().getTime()));
			result = preparedStatement.executeUpdate();
			if (result == 0) {
				throw new DaoException("Can't add news");
			}
		} catch (SQLException e) {
			throw new DaoException("Can't add news", e);
		} finally {
			closeResources(preparedStatement, connection);
		}
		return news.getId();
	}

	@Override
	public void editNews(News news) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		int result;
		try {
			preparedStatement = connection.prepareStatement(UPDATE_NEWS_SQL);
			preparedStatement.setString(1, news.getTitle());
			preparedStatement.setString(2, news.getShortText());
			preparedStatement.setString(3, news.getFullText());
			preparedStatement.setDate(4, new Date(news.getModificationDate().getTime()));
			preparedStatement.setLong(5, news.getId());
			result = preparedStatement.executeUpdate();
			if (result == 0) {
				throw new DaoException("There is no news having id = " + news.getId());
			}
		} catch (SQLException e) {
			throw new DaoException("Can't edit news", e);
		} finally {
			closeResources(preparedStatement, connection);
		}
	}

	@Override
	public void deleteNews(Long newsId) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		int result;
		try {
			preparedStatement = connection.prepareStatement(DELETE_NEWS_SQL);
			preparedStatement.setLong(1, newsId);
			result = preparedStatement.executeUpdate();
			if (result == 0) {
				throw new DaoException("There is no news having id = " + newsId);
			}
		} catch (SQLException e) {
			throw new DaoException("Can't delete news", e);
		} finally {
			closeResources(preparedStatement, connection);
		}
	}

	@Override
	public List<News> getAllNews(int beginRow, int endRow) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<News> newsList = new ArrayList<>();
		News news;
		try {
			preparedStatement = connection.prepareStatement(SELECT_ALL_NEWS_SQL);
			preparedStatement.setInt(1, beginRow);
			preparedStatement.setInt(2, endRow);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				news = new News();
				news.setId(resultSet.getLong("NEWS_ID"));
				news.setTitle(resultSet.getString("TITLE"));
				news.setShortText(resultSet.getString("SHORT_TEXT"));
				news.setFullText(resultSet.getString("FULL_TEXT"));
				news.setCreationDate(resultSet.getDate("CREATION_DATE"));
				news.setModificationDate(resultSet.getDate("MODIFICATION_DATE"));
				newsList.add(news);
			}
		} catch (SQLException e) {
			throw new DaoException("Can't get news", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
		return newsList;
	}

	@Override
	public News getNewsById(Long newsId) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		News news = new News();
		try {
			preparedStatement = connection.prepareStatement(SELECT_NEWS_BY_ID_SQL);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next() == false) {
				throw new DaoException("There is no news having id = " + news.getId());
			} else {
				news.setId(resultSet.getLong("NEWS_ID"));
				news.setTitle(resultSet.getString("TITLE"));
				news.setShortText(resultSet.getString("SHORT_TEXT"));
				news.setFullText(resultSet.getString("FULL_TEXT"));
				news.setCreationDate(resultSet.getDate("CREATION_DATE"));
				news.setModificationDate(resultSet.getDate("MODIFICATION_DATE"));
			}
		} catch (SQLException e) {
			throw new DaoException("Can't get news", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
		return news;
	}

	@Override
	public List<News> getNewsByAuthorAndTags(Long authorId, List<Long> tagIdList, int beginRow, int endRow) throws DaoException {
		final String selectNewsByAuthorAndTagsSql = SELECT_NEWS_BY_AUTHOR_AND_TAGS_BEGIN
				+ buildTagsBraces(tagIdList.size()) + SELECT_NEWS_BY_AUTHOR_AND_TAGS_END;
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<News> newsList = new ArrayList<>();
		News news = null;
		try {
			preparedStatement = connection.prepareStatement(selectNewsByAuthorAndTagsSql);
			for (int i = 0; i < tagIdList.size(); i++) {
				preparedStatement.setLong(i + 1, tagIdList.get(i));
			}
			preparedStatement.setLong(tagIdList.size() + 1, authorId);
			preparedStatement.setInt(tagIdList.size() + 2, tagIdList.size());
			preparedStatement.setInt(tagIdList.size() + 3, beginRow);
			preparedStatement.setInt(tagIdList.size() + 4, endRow);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				news = new News();
				news.setId(resultSet.getLong("NEWS_ID"));
				news.setTitle(resultSet.getString("TITLE"));
				news.setShortText(resultSet.getString("SHORT_TEXT"));
				news.setFullText(resultSet.getString("FULL_TEXT"));
				news.setCreationDate(resultSet.getDate("CREATION_DATE"));
				news.setModificationDate(resultSet.getDate("MODIFICATION_DATE"));
				newsList.add(news);
			}
		} catch (SQLException e) {
			throw new DaoException("Can't get news", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
		return newsList;
	}

	@Override
	public List<News> getNewsByAuthor(Long authorId, int beginRow, int endRow) throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<News> newsList = new ArrayList<>();
		News news = null;
		try {
			preparedStatement = connection.prepareStatement(SELECT_NEWS_BY_AUTHOR_SQL);
			preparedStatement.setLong(1, authorId);
			preparedStatement.setInt(2, beginRow);
			preparedStatement.setInt(3, endRow);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				news = new News();
				news.setId(resultSet.getLong("NEWS_ID"));
				news.setTitle(resultSet.getString("TITLE"));
				news.setShortText(resultSet.getString("SHORT_TEXT"));
				news.setFullText(resultSet.getString("FULL_TEXT"));
				news.setCreationDate(resultSet.getDate("CREATION_DATE"));
				news.setModificationDate(resultSet.getDate("MODIFICATION_DATE"));
				newsList.add(news);
			}
		} catch (SQLException e) {
			throw new DaoException("Can't get news", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
		return newsList;
	}

	@Override
	public List<News> getNewsByTags(List<Long> tagIdList, int beginRow, int endRow) throws DaoException {
		final String selectNewsByTagsSql = SELECT_NEWS_BY_TAGS_BEGIN + buildTagsBraces(tagIdList.size())
				+ SELECT_NEWS_BY_TAGS_END;
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<News> newsList = new ArrayList<>();
		News news;
		try {
			preparedStatement = connection.prepareStatement(selectNewsByTagsSql);
			for (int i = 0; i < tagIdList.size(); i++) {
				preparedStatement.setLong(i + 1, tagIdList.get(i));
			}
			preparedStatement.setInt(tagIdList.size() + 1, tagIdList.size());
			preparedStatement.setInt(tagIdList.size() + 2, beginRow);
			preparedStatement.setInt(tagIdList.size() + 3, endRow);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				news = new News();
				news.setId(resultSet.getLong("NEWS_ID"));
				news.setTitle(resultSet.getString("TITLE"));
				news.setShortText(resultSet.getString("SHORT_TEXT"));
				news.setFullText(resultSet.getString("FULL_TEXT"));
				news.setCreationDate(resultSet.getDate("CREATION_DATE"));
				news.setModificationDate(resultSet.getDate("MODIFICATION_DATE"));
				newsList.add(news);
			}
		} catch (SQLException e) {

			throw new DaoException("Can't get news", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
		return newsList;
	}

	@Override
	public int getAllNewsCount() throws DaoException {
		return getNewsCount(SELECT_ALL_NEWS_COUNT_SQL);
	}

	@Override
	public int getNewsByAuthorAndTagsCount(Long authorId, List<Long> tagIdList) throws DaoException {
		final String selectNewsByAuthorAndTagsCountSql = SELECT_NEWS_BY_AUTHOR_AND_TAGS_COUNT_BEGIN
				+ buildTagsBraces(tagIdList.size()) + SELECT_NEWS_BY_AUTHOR_AND_TAGS_COUNT_END;
		int count = 0;
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = connection.prepareStatement(selectNewsByAuthorAndTagsCountSql);
			for (int i = 0; i < tagIdList.size(); i++) {
				preparedStatement.setLong(i + 1, tagIdList.get(i));
			}
			preparedStatement.setLong(tagIdList.size() + 1, authorId);
			preparedStatement.setInt(tagIdList.size() + 2, tagIdList.size());
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			count = resultSet.getInt(1);
		} catch (SQLException e) {
			throw new DaoException("Can't get news count", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
		return count;
	}

	@Override
	public int getNewsByAuthorCount(Long authorId) throws DaoException {
		final String selectNewsByAuthorCountSql = SELECT_NEWS_BY_AUTHOR_COUNT;
		int count = 0;
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = connection.prepareStatement(selectNewsByAuthorCountSql);
			preparedStatement.setLong(1, authorId);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			count = resultSet.getInt(1);
		} catch (SQLException e) {
			throw new DaoException("Can't get news count", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
		return count;
	}

	@Override
	public int getNewsByTagsCount(List<Long> tagIdList) throws DaoException {
		final String selectNewsByTagsCountSql = SELECT_NEWS_BY_TAGS_COUNT_BEGIN
				+ buildTagsBraces(tagIdList.size()) + SELECT_NEWS_BY_TAGS_COUNT_END;
		int count = 0;
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = connection.prepareStatement(selectNewsByTagsCountSql);
			for (int i = 0; i < tagIdList.size(); i++) {
				preparedStatement.setLong(i + 1, tagIdList.get(i));
			}
			preparedStatement.setInt(tagIdList.size() + 1, tagIdList.size());
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			count = resultSet.getInt(1);
		} catch (SQLException e) {
			throw new DaoException("Can't get news count", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
		return count;
	}

	private int getNewsCount(String sql) throws DaoException {
		int count = 0;
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = connection.prepareStatement(sql);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			count = resultSet.getInt(1);
		} catch (SQLException e) {
			throw new DaoException("Can't get news count", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
		return count;
	}
	
	private String buildTagsBraces(int tagCount) {
		StringBuilder braces = new StringBuilder("(");
		for (int i = 0; i < tagCount; i++) {
			braces.append("?");
			if (i < tagCount - 1) {
				braces.append(",");
			}
		}
		braces.append(")");
		return braces.toString();
	}

	private Long getNextNewsId() throws DaoException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = connection.prepareStatement(GET_NEXT_NEWS_ID_SQL);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			return resultSet.getLong(1);
		} catch (SQLException e) {
			throw new DaoException("Can't get next news id", e);
		} finally {
			closeResources(resultSet, preparedStatement, connection);
		}
	}

}
