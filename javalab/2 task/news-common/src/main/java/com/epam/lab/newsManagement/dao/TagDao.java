package com.epam.lab.newsManagement.dao;

import java.util.List;

import com.epam.lab.newsManagement.entity.Tag;

public interface TagDao {

	List<Tag> getAllTags() throws DaoException;
	
	List<Tag> getTagsByNews(Long newsId) throws DaoException;
	
	void addTagToNews(Long newsId, Long tagId) throws DaoException;
	
	void removeTagsFromNews(Long newsId) throws DaoException;
	
	Long addTag(Tag tag) throws DaoException;
	
	void editTag(Tag tag) throws DaoException;

	void deleteTag(Long tagId) throws DaoException;
	
}
