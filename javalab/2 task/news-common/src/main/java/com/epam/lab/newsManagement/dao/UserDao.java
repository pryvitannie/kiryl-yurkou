package com.epam.lab.newsManagement.dao;

import com.epam.lab.newsManagement.entity.User;

public interface UserDao {

	Long addUser(User user) throws DaoException;

	void deleteUser(Long userId) throws DaoException;

}
