package com.epam.lab.newsManagement.webutil;

public class PaginationHelper {

	private int firstPage;	
	private int visiblePagesCount;
	private int itemsPerPage;
	private int pagesCount;

	public void initizialize(int fullNewsListSize) {
		int pagesCount = fullNewsListSize / itemsPerPage;
		if (fullNewsListSize % itemsPerPage != 0) {
			pagesCount++;
		}
		this.pagesCount = pagesCount;
	}
	
	public VisiblePageBorders calculateVisiblePageNumberBorders(int currentPageNumber) {
		int beginPageNumber;
		int endPageNumber;
		VisiblePageBorders visiblePageBorders = new VisiblePageBorders();
		if (currentPageNumber <= pagesCount / 2) {
			if (currentPageNumber - visiblePagesCount / 2 >= firstPage) {
				beginPageNumber = currentPageNumber - visiblePagesCount / 2;
				if (currentPageNumber + visiblePagesCount / 2 <= pagesCount) {
					endPageNumber = currentPageNumber + visiblePagesCount / 2;
				} else {
					endPageNumber = pagesCount;
				}
			} else {
				beginPageNumber = firstPage;
				if (beginPageNumber + visiblePagesCount - 1 <= pagesCount) {
					endPageNumber = beginPageNumber + visiblePagesCount - 1;
				} else {
					endPageNumber = pagesCount;
				}
			}
		} else {
			if (currentPageNumber + visiblePagesCount / 2 <= pagesCount) {
				endPageNumber = currentPageNumber + visiblePagesCount / 2;
				if (currentPageNumber - visiblePagesCount / 2 >= firstPage) {
					beginPageNumber = currentPageNumber - visiblePagesCount / 2;
				} else {
					beginPageNumber = firstPage;
				}
			} else {
				endPageNumber = pagesCount;
				if (endPageNumber - visiblePagesCount + 1 >= firstPage) {
					beginPageNumber = endPageNumber - visiblePagesCount + 1;
				} else {
					beginPageNumber = firstPage;
				}
			}
		}
		visiblePageBorders.setBeginPageNumber(beginPageNumber);
		visiblePageBorders.setEndPageNumber(endPageNumber);
		return visiblePageBorders;
	}
	
	public int correctPageNumber(int currentPageNumber) {
		if (currentPageNumber < firstPage) {
			return firstPage;
		}
		if (currentPageNumber > pagesCount) {
			return pagesCount;
		}
		return currentPageNumber;
	}
	
	public int getBeginRow(int currentPage) {
		return itemsPerPage * (currentPage - 1) + 1;
	}
	
	public int getEndRow(int currentPage) {
		return itemsPerPage * currentPage;
	}
	
	/*public <T> List<T> getCurrentPageItemsList(List<T> fullItemsList, int currentPageNumber) {
		List<T> currentPageNewsList = new ArrayList<>();
		int finishIndex = currentPageNumber * getItemsPerPage();
		int startIndex = finishIndex - getItemsPerPage();
		if (finishIndex > fullItemsList.size()) {
			finishIndex = fullItemsList.size();
		}
		for (int i = startIndex; i < finishIndex; i++) {
			currentPageNewsList.add(fullItemsList.get(i));
		}	
		return currentPageNewsList;
	}*/

	public int getFirstPage() {
		return firstPage;
	}

	public void setFirstPage(int firstPage) {
		this.firstPage = firstPage;
	}

	public int getVisiblePagesCount() {
		return visiblePagesCount;
	}

	public void setVisiblePagesCount(int visiblePagesCount) {
		this.visiblePagesCount = visiblePagesCount;
	}

	public int getItemsPerPage() {
		return itemsPerPage;
	}

	public void setItemsPerPage(int itemsPerPage) {
		this.itemsPerPage = itemsPerPage;
	}

	public int getPagesCount() {
		return pagesCount;
	}

	public void setPagesCount(int pagesCount) {
		this.pagesCount = pagesCount;
	}
	
}
