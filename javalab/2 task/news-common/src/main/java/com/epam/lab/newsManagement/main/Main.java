package com.epam.lab.newsManagement.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.lab.newsManagement.entity.News;
import com.epam.lab.newsManagement.service.NewsService;
import com.epam.lab.newsManagement.service.ServiceException;
import com.epam.lab.newsManagement.service.util.SearchCriteria;

public class Main {
	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("beans.xml");
		NewsService newsService = (NewsService) applicationContext.getBean("newsService");
		try {
			for (News news : newsService.search(new SearchCriteria(), 5, 6)) {
				System.out.println(news.getTitle());
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
