package com.epam.lab.newsManagement.service;

import java.util.List;

import com.epam.lab.newsManagement.entity.Comment;

public interface CommentService {

	boolean addComment(String text, Long newsId) throws ServiceException;

	boolean deleteComment(Long commentId) throws ServiceException;

	List<Comment> getCommentsByNews(Long newsId) throws ServiceException;

}
